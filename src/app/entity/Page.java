package app.entity;

import javafx.scene.control.Label;

public class Page {
    public Page(String numberPage, String urlPage) {
        this.numberPage = numberPage;
        this.urlPage = urlPage;
    }

    private String numberPage;
    private String urlPage;
    private boolean isSkip;


    public String getNumberPage() {
        return numberPage;
    }

    public void setNumberPage(String numberPage) {
        this.numberPage = numberPage;
    }

    public String getUrlPage() {
        return urlPage;
    }

    public void setUrlPage(String urlPage) {
        this.urlPage = urlPage;
    }

    public void setSkip(boolean skip) {
        isSkip = skip;
    }

    public boolean isSkip() {
        return isSkip;
    }

    public boolean isActive() {
        return urlPage == null;
    }


}
