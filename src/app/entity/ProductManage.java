package app.entity;

public class ProductManage extends Product {
    private String mainImage;
    private String stringPrice;
    private String stock;
    private String timeExpired;
    private String javascriptDelete;

    public String getMainImage() {
        return mainImage;
    }

    public void setMainImage(String mainImage) {
        this.mainImage = mainImage;
    }

    public String getStringPrice() {
        return stringPrice;
    }

    public void setStringPrice(String stringPrice) {
        this.stringPrice = stringPrice;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getTimeExpired() {
        return timeExpired;
    }

    public void setTimeExpired(String timeExpired) {
        this.timeExpired = timeExpired;
    }

    public String getJavascriptDelete() {
        return javascriptDelete;
    }

    public void setJavascriptDelete(String javascriptDelete) {
        this.javascriptDelete = javascriptDelete;
    }
}
