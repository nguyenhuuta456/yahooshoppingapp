package app.entity;

import com.google.gson.annotations.SerializedName;

public class Product {
    @SerializedName("name")
    String name;
    @SerializedName("price")
    int price;
    @SerializedName("quantity")
    int quantity;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


}
