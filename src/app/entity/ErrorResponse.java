package app.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ErrorResponse {
    @SerializedName("errors")
    private List<ErrorData> errors;

    public class ErrorData {

        @SerializedName("code")
        private String code;
        @SerializedName("message")
        private String message;

        public String getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }
    }

    public ErrorData getErrors() {
        if (errors != null && errors.size() > 0)
            return errors.get(0);
        return null;
    }
}
