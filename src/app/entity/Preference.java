package app.entity;

public class Preference {
    private String host;
    private String accountId;
    private String accountApp;
    private String passwordApp;

    private String accountYahoo;
    private String passwordYahoo;
    private String shopId;

    private String urlLoginYahoo;


    public Preference(String host, String accountId, String accountApp, String passwordApp, String accountYahoo, String passwordYahoo) {
        this.host = host;
        this.accountId = accountId;
        this.accountApp = accountApp;
        this.passwordApp = passwordApp;
        this.accountYahoo = accountYahoo;
        this.passwordYahoo = passwordYahoo;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAccountId() {
        return accountId;
    }

    public String getAccountApp() {
        return accountApp;
    }

    public void setAccountApp(String accountApp) {
        this.accountApp = accountApp;
    }

    public String getPasswordApp() {
        return passwordApp;
    }

    public void setPasswordApp(String passwordApp) {
        this.passwordApp = passwordApp;
    }

    public String getAccountYahoo() {
        return accountYahoo;
    }

    public void setAccountYahoo(String accountYahoo) {
        this.accountYahoo = accountYahoo;
    }

    public String getPasswordYahoo() {
        return passwordYahoo;
    }

    public void setPasswordYahoo(String passwordYahoo) {
        this.passwordYahoo = passwordYahoo;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String urlLoginYahoo) {
        this.host = urlLoginYahoo;
    }

    public boolean isLogin() {
        return accountApp != null;
    }

    public String getUrlLoginYahoo() {
        return urlLoginYahoo;
    }

    public void setUrlLoginYahoo(String urlLoginYahoo) {
        this.urlLoginYahoo = urlLoginYahoo;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getShopId() {
        return shopId;
    }

    @Override
    public String toString() {
        return "Preference{" +
                "host='" + host + '\'' +
                ", shopId='" + shopId + '\'' +
                ", accountId='" + accountId + '\'' +
                ", accountApp='" + accountApp + '\'' +
                ", passwordApp='" + passwordApp + '\'' +
                ", accountYahoo='" + accountYahoo + '\'' +
                ", passwordYahoo='" + passwordYahoo + '\'' +
                ", urlLoginYahoo='" + urlLoginYahoo + '\'' +
                '}';
    }
}
