package app.entity;

import app.utils.Constants;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ProductUpload extends Product {
    public static final int STATUS_DEFAULT = -1;
    public static final int STATUS_UPLOAD = 0;
    public static final int STATUS_DONE = 1;
    public static final int UPLOAD_FAILURE = 2;
    public static final int STATUS_EDIT = 8;

    public static final int STATUS_SYSTEM_ERROR = 9;

    @SerializedName("id")
    private String id;
    @SerializedName("item_code")
    private String itemCode;
    @SerializedName("sale_period_start")
    private String startTime;
    @SerializedName("sale_period_end")
    private String endTime;
    @SerializedName("explanation")
    private String description1;
    @SerializedName("caption")
    private String description2;//allow html
    @SerializedName("path")
    private String path;
    @SerializedName("product_category")
    private String productCategory;
    @SerializedName("image_id")
    private List<String> images;
    /*
     * 0 : new
     * 1 : old
     * */
    @SerializedName("item_condition")
    private int itemCondition;
    /*
     * -1 : default
     * 0 : upload
     * 2 : failure
     * 1 : done
     * 8 : edit
     *
     * */
    @SerializedName("publish_status")
    private int publishStatus;

    /*
     * 0 : has tax
     * 1 : no tex
     * */
    private boolean tax;
    private boolean isPrivate;






    /*========================*/

    private StringBuilder messageFailure;


    public void setMessageFailure(String message) {
        if (messageFailure == null) {
            messageFailure = new StringBuilder();
        }
        messageFailure.append(message);
        messageFailure.append("\n");
    }

    public String getMessageFailure() {
        if (messageFailure == null) {
            return Constants.EMPTY;
        }
        return messageFailure.toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }


    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getDescription1() {
        return description1;
    }

    public void setDescription1(String description1) {
        this.description1 = description1;
    }

    public String getDescription2() {
        return description2;
    }

    public void setDescription2(String description2) {
        this.description2 = description2;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public boolean isTax() {
        return tax;
    }

    public void setTax(boolean tax) {
        this.tax = tax;
    }

    public boolean isNew() {
        return itemCondition == 0;
    }

    public void setNew(int aNew) {
        itemCondition = aNew;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }


    public int getPublishStatus() {
        return publishStatus;
    }

    public void setPublishStatus(int publishStatus) {
        this.publishStatus = publishStatus;
    }


    @Override
    public String toString() {
        return "ProductUpload{" +
                "id='" + id + '\'' +
                ", itemCode='" + itemCode + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", description1='" + description1 + '\'' +
                ", description2='" + description2 + '\'' +
                ", quantity=" + quantity +
                ", path='" + path + '\'' +
                ", productCategory='" + productCategory + '\'' +
                ", images=" + images +
                ", itemCondition=" + itemCondition +
                ", publishStatus=" + publishStatus +
                '}';
    }
}
