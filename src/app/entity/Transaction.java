package app.entity;

import java.util.List;
import java.util.Map;

public class Transaction {

    private String time;

    private String orderNumber;

    private String productName;

    private String userName;

    private String price;

    private String imageStatus;

    private String statusProduct;

    private String urlDetailProduct;

    private List<Map<String, String>> listPage;


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImageStatus() {
        return imageStatus;
    }

    public void setImageStatus(String imageStatus) {
        this.imageStatus = imageStatus;
    }

    public String getStatusProduct() {
        return statusProduct;
    }

    public void setStatusProduct(String statusProduct) {
        this.statusProduct = statusProduct;
    }

    public String getUrlDetailProduct() {
        return urlDetailProduct;
    }

    public void setUrlDetailProduct(String urlDetailProduct) {
        this.urlDetailProduct = urlDetailProduct;
    }

    public List<Map<String, String>> getListPage() {
        return listPage;
    }

    public void setListPage(List<Map<String, String>> listPage) {
        this.listPage = listPage;
    }
}
