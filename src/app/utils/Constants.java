package app.utils;

import java.util.ArrayList;
import java.util.List;

public class Constants {
    public static final int DATABASE_VERSION = 1;
    public static final String APP_VERSION_CODE = "5.0.4";

    public static final String EMPTY = "";
    public static final String SPACE_CHARACTER = " ";

    public static final String LOG4J_CONFIG_FILE_NAME = "log4j2.xml";
    public static final String CHROME_OPTION_DISABLE_NOTIFICATIONS = "--disable-notifications";

    public static final String CHROME_OPTION_IN_PRIVATE_MODE = "--incognito";

    public static final int WEB_DRIVER_WINDOW_WIDTH = 1920;

    public static final int WEB_DRIVER_WINDOW_HEIGHT = 1080;


    public static final String AppKey = "e7plgnvwz1od94g";//"v1v5azcbisjh0qw";

    public static final String AppSecret = "y7gnpdix8w8dia3";//"uwfdcrjc9b6jdxt";

    public static final String AccessKey = "GQssbEgEOUAAAAAAAAAACtlk71dkf2ZZsgUEw-5FqIHOZAAOHt4dkTpcYAwS1Nd8";//"J2WDv39_zrAAAAAAAAAAX4QO2UFS-owjq8h-ZjfedqIhLKxiqNDmfAlm0DbKepfk";

    public static final String URL_DOWNLOAD_UPGRADE = "https://drive.google.com/drive/folders/1XwN8Tgy8PF2TtAyP-7AuPkNf38YXaPW7";


    public static class Server {

        public static List<String> getListServer() {
            List<String> listServer = new ArrayList<>();
            for (int i = 0; i < 13; i++) {
                listServer.add(getServer(i + 1));
            }
            return listServer;
        }

        private static String getServer(int idServer) {
            switch (idServer) {
                case 1:
                    return SERVER_1;
                case 2:
                    return SERVER_2;
                case 3:
                    return SERVER_3;
                case 4:
                    return SERVER_4;
                case 5:
                    return SERVER_5;
                case 6:
                    return SERVER_6;
                case 7:
                    return SERVER_7;
                case 8:
                    return SERVER_8;
                case 9:
                    return SERVER_9;
                case 10:
                    return SERVER_10;
                case 11:
                    return SERVER_11;
                case 12:
                    return SERVER_12;
                case 13:
                    return SERVER_13;
                default:
                    return SERVER_12;
            }
        }

        private static final String SERVER_1 = "133.130.71.169_nonstock2.watermeru.com";
        private static final String SERVER_2 = "133.130.74.200_autoslider.net";
        private static final String SERVER_3 = "133.130.76.214_nonstock5.watermeru.com";
        private static final String SERVER_4 = "157.7.129.188_nonstock4.watermeru.com";
        private static final String SERVER_5 = "157.7.133.227_nonstock3.watermeru.com";
        private static final String SERVER_6 = "157.7.135.241_nonstok.watermeru.com";
        private static final String SERVER_7 = "157.7.136.14";
        private static final String SERVER_8 = "157.7.136.14_autostocker.net";
        private static final String SERVER_9 = "157.7.140.197_nonstock1.watermeru.com";
        private static final String SERVER_10 = "157.7.222.184_autotype1.net";
        private static final String SERVER_11 = "157.7.243.201_tool.muzaiko-lab.com";
        private static final String SERVER_12 = "ServerDev_133.130.73.227";
        private static final String SERVER_13 = "ServerStaging_157.7.243.201";
    }

}
