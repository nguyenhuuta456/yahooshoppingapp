package app.utils;

import app.App;
import app.exceptions.WebDriverNotFoundException;
import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;

/**
 * Created by HieuPT on 2/2/2018.
 */
public class WebDriverUtils {

    private static final String TAG = WebDriverUtils.class.getSimpleName();

    private static WebDriverUtils instance;

    public static synchronized WebDriverUtils getInstance() {
        if (instance == null) {
            instance = new WebDriverUtils();
        }
        return instance;
    }

    private WebDriver webDriver;


    public WebDriver createWebDriver() {
        try {
            ChromeOptions chromeOptions = new ChromeOptions();
            if (!App.debug) {
                chromeOptions.addArguments(Constants.CHROME_OPTION_DISABLE_NOTIFICATIONS);
                chromeOptions.addArguments(Constants.CHROME_OPTION_IN_PRIVATE_MODE);
                String baseLocation = System.getProperty("user.dir") + "/webdriver/chrome";
                if (SystemUtils.IS_OS_WINDOWS) {
                    chromeOptions.setBinary(new File(baseLocation + "/windows/App/Chrome-bin", "chrome.exe"));
                } else if (SystemUtils.IS_OS_MAC) {
                    chromeOptions.setBinary(new File(baseLocation + "/mac/Google Chrome.app/Contents/MacOS", "Google Chrome"));
                }
                chromeOptions.setHeadless(true);
            }
            webDriver = new ChromeDriver(chromeOptions);
        } catch (WebDriverException e) {
            try {
                throw new WebDriverNotFoundException("Chrome is not found.", e);
            } catch (WebDriverNotFoundException e1) {
                e1.printStackTrace();
            }
        }
        webDriver.manage().window().setSize(new Dimension(Constants.WEB_DRIVER_WINDOW_WIDTH, Constants.WEB_DRIVER_WINDOW_HEIGHT));
        return webDriver;
    }


    public static boolean isElementPresentAndDisplayed(WebElement element) {
        try {
            if (element != null) {
                return element.isDisplayed();
            }
        } catch (WebDriverException ignored) {
        }
        return false;
    }

    private WebDriverUtils() {
    }
}
