package app.utils.dialog;

import javafx.scene.control.Label;

public class DialogAppController {


    public Label labelContent;

    public void initData(String content) {
        labelContent.setText(content);
    }
}
