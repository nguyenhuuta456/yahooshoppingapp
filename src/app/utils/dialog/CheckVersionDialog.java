package app.utils.dialog;

import app.App;
import app.utils.Constants;
import app.utils.StringConstants;
import app.utils.Utils;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class CheckVersionDialog {
    @FXML
    private Button btnDownload;

    private Stage newWindow;

    public CheckVersionDialog(Stage stage) {
        initView(stage);
        initAction();
    }

    private void initView(Stage stage) {
        if (stage != null) {
            Parent root;
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout/dialog_check_version.fxml"));
                loader.setController(this);
                root = loader.load();
                Scene secondScene = new Scene(root);
                newWindow = new Stage();
                newWindow.setScene(secondScene);
                newWindow.setTitle(StringConstants.CHECK_VERSION);
                newWindow.initModality(Modality.WINDOW_MODAL);
                //newWindow.initStyle(StageStyle.UNDECORATED);
                newWindow.initOwner(stage);
                newWindow.getScene().getWindow().centerOnScreen();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void initAction() {
        btnDownload.setOnMouseClicked(event -> {
            Utils.openBrowser(Constants.URL_DOWNLOAD_UPGRADE);
            try {
                App.getInstance().stop();
            } catch (Exception e) {
                e.printStackTrace();
            }

        });
    }

    public void show() {
        newWindow.show();
    }

    public void hide() {
        newWindow.hide();
    }

}
