package app.utils.dialog;


import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.stage.Modality;

import java.io.IOException;

public class DialogApp extends Dialog {
    private DialogAppController dialogController;
    private Button buttonOk, buttonClose;
    IYahooDialogCallback callback;

    public DialogApp() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout/layout_dialog_app.fxml"));
            initModality(Modality.APPLICATION_MODAL);
            Parent root = loader.load();
            dialogController = loader.getController();
            setTitle("Notification");
            getDialogPane().setContent(root);
            getDialogPane().getButtonTypes().addAll(ButtonType.OK);
            buttonOk = (Button) getDialogPane().lookupButton(ButtonType.OK);
            buttonOk.setText("はい");
            buttonOk.addEventFilter(ActionEvent.ACTION, event -> {
                if (callback != null) {
                    callback.actionButtonOK();
                }
                close();
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addButtonCancel(String buttonTry, String buttonCancel) {
        this.callback = callback;
        getDialogPane().getButtonTypes().addAll(ButtonType.CLOSE);
        buttonOk.setText(buttonTry);
        buttonClose = (Button) getDialogPane().lookupButton(ButtonType.CLOSE);
        buttonClose.setText(buttonCancel);
        buttonClose.addEventFilter(ActionEvent.ACTION, event -> {
            callback.actionButtonClose();
            close();
        });
    }

    public void addButtonCancel(String content, String buttonTry, String buttonCancel, IYahooDialogCallback callback) {
        this.callback = callback;
        dialogController.initData(content);
        getDialogPane().getButtonTypes().addAll(ButtonType.CLOSE);
        buttonOk.setText(buttonTry);
        buttonClose = (Button) getDialogPane().lookupButton(ButtonType.CLOSE);
        buttonClose.setText(buttonCancel);
        buttonClose.addEventFilter(ActionEvent.ACTION, event -> {
            callback.actionButtonClose();
            close();
        });
    }

    public void initData(String content) {
        if (dialogController != null) {
            dialogController.initData(content);
        }
    }

    public void setCallback(IYahooDialogCallback callback) {
        this.callback = callback;
    }

    public interface IYahooDialogCallback {
        void actionButtonOK();

        void actionButtonClose();
    }
}
