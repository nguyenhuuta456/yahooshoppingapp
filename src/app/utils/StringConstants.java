package app.utils;

public class StringConstants {

    private static StringConstants stringConstant;

    public synchronized static StringConstants getInstance() {
        if (stringConstant == null) {
            stringConstant = new StringConstants();
        }
        return stringConstant;
    }

    public static final String TITLE = "タイトル";
    public static final String EDIT = "編集";
    public static final String ADD = "新規追加";
    public static final String IMAGE = "画像";
    public static final String QUATITY = "在庫";
    public static final String PRICE = "販売価格";
    public static final String PRICE_AUCTION_CURRENT = "現在の価格";
    public static final String PRICE_AUCTION_BIDOR = "即決価格";
    public static final String PROFIT = "利益";
    public static final String END_DATE = "終了日";
    public static final String ACTION = "履歴";
    public static final String OPERATING = "操作";
    public static final String PRODUCT_NAME = "商品名";
    public static final String CHANGE_AUCTION = "オークション変更";
    public static final String PUBLISH = "出品する";
    public static final String COMMENT = "コメント";
    public static final String TIME = "時間";
    public static final String RATE = "評価";
    public static final String MESSAGE = "メッセージ";
    public static final String CHECK_VERSION = "新しいバージョン";

    public static final String VERY_GOOD = "非常に良い";
    public static final String GOOD = "良い";
    public static final String NORMAL = "どちらでもない";
    public static final String BAD = "悪い";
    public static final String VERY_BAD = "非常に悪い";

    public static final String PLEASE_ENTER_ENOUGH_INFO = "入力情報が不足です";
    public static final String SEND_MESSAGE_TRANSACTION_ERROR = "取引メッセージを送信できませんでした。";
    public static final String PLEASE_ENTER = "を入力してくたさい。";

    public static final String TITLE_DIALOG_SAVE_MESSAGE_PATTERN = "定型文登録";
    public static final String TITLE_TAB_MESSAGE_PATTERN = "定型文一覧";

    public static final String STATUS_NOT_PUBLISH_AMAZON = "出品停止";
    public static final String STATUS_PUBLISHED_AMAZON = "出品済み";

    public String url_not_null = "サーバーURLを入力してください。";
    public String user_name_not_null = "アカウントを入力してください";
    public String password_not_null = "パスワードを入力してください";
    public String status_prepare_upload = "出品処理中";
    public String status_uploaded_amazon = "出品済み";
    public String status_upload_failure_amazon = "出品できませんでした。";
    public String status_stop_upload = "出品終了";
    public String try_again = "リトライ";
    public String cancel = "キャンセル";
    public String failure = "エラー";
    public String refresh_token_failure = "Refresh token failure";

}
