package app.utils;

import org.omg.CORBA.PUBLIC_MEMBER;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConvert {
    public static final String FORMAT_1 = "yyyy/MM/dd";
    public static final String FORMAT_2 = "yyyy-MM-dd'T'HH:mm:ss";

    public static final String formatDate(long timeStamp, String typeFormat) {
        Date date = new Date();
        SimpleDateFormat dfDate = new SimpleDateFormat(typeFormat);
        if (timeStamp != 0) {
            date.setTime(timeStamp);
            return dfDate.format(date);
        }
        return Constants.EMPTY;
    }

    public static final String formatDate(String timeStamp, String fromFormat, String toFormat) {
        SimpleDateFormat dfDate = new SimpleDateFormat(fromFormat);
        if (timeStamp != null && !timeStamp.isEmpty()) {
            try {
                Date date = dfDate.parse(timeStamp);
                return formatDate(date.getTime(), toFormat);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return Constants.EMPTY;
    }

}
