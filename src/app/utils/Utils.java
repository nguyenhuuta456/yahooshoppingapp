package app.utils;

import app.entity.ErrorResponse;
import com.google.gson.Gson;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import okhttp3.ResponseBody;
import org.apache.commons.codec.binary.Base64;

import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermission;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public class Utils {

    public static final String formatDate1 = "yyyy-MM-dd HH:mm:ss";
    public static final String formatDate2 = "yyyy-MM-dd";
    public static final String formatDate3 = "yyyyMMdd";
    public static final String formatDate4 = "yyyyMMddHHmm";
    public static final String formatDate5 = "yyyy-MM-dd'T'HH:mm:ss.SSSX";
    public static final String formatDate6 = "HH:mm";

    public static void postDelay(IDoWordCallback doWork, long milliSeconds) {
        Task<Void> sleeper = new Task<Void>() {

            @Override
            protected Void call() {
                try {
                    Thread.sleep(milliSeconds);
                } catch (InterruptedException e) {
                }
                return null;
            }
        };
        sleeper.setOnSucceeded(event -> doWork.run());
        new Thread(sleeper).start();
    }

    public static void postDelayy() {

        KeyFrame kf2 = new KeyFrame(Duration.millis(300), e -> {
            //TODO
        });
        Timeline timeline = new Timeline(kf2);
        Platform.runLater(timeline::play);
    }

    public static void runOnUIThread(IDoWordCallback doWork) {
        Platform.runLater(doWork::run);
    }

    public static <V> void runInBackground(Callable<V> job, Consumer<V> callback) {
        Task<V> task = new Task<V>() {

            @Override
            protected V call() throws Exception {
                return job.call();
            }
        };
        task.setOnSucceeded(event -> callback.accept(task.getValue()));
        Executors.newSingleThreadExecutor().submit(task);
    }

    public static String getStringError(ResponseBody bodyError) {
        try {
            Gson gson = new Gson();
            ErrorResponse response = gson.fromJson(bodyError.string(), ErrorResponse.class);
            return response.getErrors().getMessage() + " - " + response.getErrors().getCode();
        } catch (Exception e) {
            System.out.println("Util - getStringError " + e.getMessage());
        }
        return StringConstants.getInstance().failure;
    }

    public static String getTimestamp() {
        String timestamp = null;
        Calendar cal = Calendar.getInstance();
        DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        dfm.setTimeZone(TimeZone.getTimeZone("GMT"));
        timestamp = dfm.format(cal.getTime());
        return timestamp;
    }

    public static String encodeURL(String string) {
        string = string.replaceAll(":", "%3A");
        return string;
    }

    public static String checkMD5(String input) {
        return computeContentMD5Header(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)));
    }

    public static String computeContentMD5Header(InputStream inputStream) {
        DigestInputStream s;
        try {
            s = new DigestInputStream(inputStream, MessageDigest.getInstance("MD5"));
            byte[] buffer = new byte[8192];
            while (s.read(buffer) > 0) ;
            return new String(Base64.encodeBase64(s.getMessageDigest().digest()), "UTF-8");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void openBrowser(String url) {
        Desktop desktop = Desktop.getDesktop();
        if (desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.browse(new URI(url));
            } catch (IOException | URISyntaxException e1) {
                e1.printStackTrace();
            }
        }
    }

    public static Date getDateFromString(String dateString, String format) {
        DateFormat df = new SimpleDateFormat(format);
        Date outDate;
        try {
            outDate = df.parse(dateString);
            return outDate;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static long getLongFromString(String dateString, String format) {
        DateFormat df = new SimpleDateFormat(format);
        Date outDate;
        try {
            outDate = df.parse(dateString);
            return outDate.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static String getStringFromDate(Date date, String format) {
        DateFormat df = new SimpleDateFormat(format);
        String reportDate = df.format(date);
        return reportDate;
    }

    public static void grantFilePermission(File file) {
        if (file != null && file.exists()) {
            try {
                Set<PosixFilePermission> perms = new HashSet<>();
                perms.add(PosixFilePermission.OWNER_READ);
                perms.add(PosixFilePermission.OWNER_WRITE);
                perms.add(PosixFilePermission.OWNER_EXECUTE);
                perms.add(PosixFilePermission.GROUP_READ);
                perms.add(PosixFilePermission.GROUP_WRITE);
                perms.add(PosixFilePermission.GROUP_EXECUTE);
                perms.add(PosixFilePermission.OTHERS_READ);
                perms.add(PosixFilePermission.OTHERS_WRITE);
                perms.add(PosixFilePermission.OTHERS_EXECUTE);
                Files.setPosixFilePermissions(file.toPath(), perms);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String convertDate(long timeStamp, String format) {
        Date date = new Date();
        SimpleDateFormat dfDate = new SimpleDateFormat(format);
        if (timeStamp != 0) {
            date.setTime(timeStamp);
            return dfDate.format(date);
        }
        return Constants.EMPTY;
    }

    public static boolean isNullEmpty(String s) {
        return (s == null || s.isEmpty());
    }

    public static boolean isURL(String url) {
        try {
            new URL(url).toURI();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static int getRandomNumber(int min, int max) {
        if (max > min) {
            Random random = new Random();
            return random.nextInt(max - min + 1) + min;
        } else {
            throw new IllegalArgumentException("Max must be greater than min");
        }
    }

    public static void loadImage(String url, ImageView imageView) {
        if (url != null && !url.isEmpty()) {
            javafx.scene.image.Image image = new Image(url, 80, 80, true, true, true);
            imageView.setImage(image);
            imageView.setCache(true);
        }
    }
}
