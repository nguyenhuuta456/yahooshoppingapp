package app.anotations;

/**
 * Created by HieuPT on 8/29/2017.
 */
public enum FormatType {
    JSON,
    XML
}
