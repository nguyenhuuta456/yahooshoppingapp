package app.anotations;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by HieuPT on 8/29/2017.
 */
@Target(METHOD)
@Retention(RUNTIME)
public @interface ResponseType {

    FormatType value() default FormatType.JSON;
}
