package app.controller.loginapp;

import app.App;
import app.controller.BaseController;
import app.controller.IBaseViewModelCallback;
import app.stage.MainStage;
import app.utils.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;

public class LoginAppController extends BaseController<LoginAppViewModel> implements IBaseViewModelCallback {
    public TextField inputAccount;
    public PasswordField inputPassword;
    public Button buttonLogin;
    public ProgressIndicator progressBar;
    public TextField inputUrl;

    @Override
    protected String TAG() {
        return this.getClass().getSimpleName();
    }

    @Override
    protected LoginAppViewModel createBaseViewModel() {
        return new LoginAppViewModel(this);
    }

    @Override
    protected void initView() {
        Logger.d(TAG(), "initView");
    }

    @Override
    protected void initAction() {

    }

    @Override
    protected void initData() {
        inputAccount.setText("");
        inputPassword.setText("");
        inputUrl.setText(App.url);
    }

    public void onEnter() {
        buttonLogin.fire();
    }

    public void onActionLogin() {
        String url = inputUrl.getText();
        String userName = inputAccount.getText();
        String password = inputPassword.getText();
        if (!url.contains("http://")) {
            url = "http://" + url;
        }
        getViewModel().loginApp(url, userName, password);
    }

    @Override
    public void progressLoading(boolean isShow) {
        progressBar.setVisible(isShow);
    }

    @Override
    public void onSuccess() {
        new MainStage(App.getInstance().getStageApp());
    }

    @Override
    public void showMessage(String message) {
        showDialog(message);
    }
}
