package app.controller.loginapp;

import app.App;
import app.controller.BaseViewModel;
import app.controller.IBaseViewModelCallback;
import app.entity.Preference;
import app.requests.IBaseCallback;
import app.requests.YahooShoppingModel;
import app.requests.response.LoginResponse;
import app.utils.Constants;
import app.utils.Utils;

class LoginAppViewModel extends BaseViewModel {
    private YahooShoppingModel yahooShoppingModel;

    public LoginAppViewModel(IBaseViewModelCallback callback) {
        super(callback);
        yahooShoppingModel = new YahooShoppingModel();
    }

    void loginApp(String host, String userName, String password) {
        String message = Constants.EMPTY;
        if (!Utils.isURL(host)) {
            message = getString().url_not_null;
        } else if (userName == null || userName.isEmpty()) {
            message = getString().user_name_not_null;
        } else if (password == null || password.isEmpty()) {
            message = getString().password_not_null;
        }
        if (message.isEmpty()) {
            getBaseCallback().progressLoading(true);
            yahooShoppingModel.loginApp(host, userName, password, new IBaseCallback<LoginResponse>() {
                @Override
                public void onResponse(LoginResponse response) {
                    getBaseCallback().progressLoading(false);
                    if (response.isSuccess()) {
                        LoginResponse.LoginData data = response.getData();
                        Preference preference = new Preference(host, data.getAccountId(), userName, password, data.getAccountYahoo(), data.getPassword());
                        App.getInstance().savePreference(preference);
                        App.getInstance().setPreference(preference);
                        getBaseCallback().onSuccess();
                    } else {
                        getBaseCallback().showMessage(response.getMessage());
                    }

                }

                @Override
                public void onError(String message) {
                    getBaseCallback().progressLoading(false);
                    getBaseCallback().showMessage(message);
                }
            });
        } else {
            getBaseCallback().showMessage(message);
        }
    }
}
