package app.controller.loginapp;

import app.controller.IBaseViewModelCallback;

public interface ILoginAppCallback extends IBaseViewModelCallback {
    void loginFailure(String message);

}
