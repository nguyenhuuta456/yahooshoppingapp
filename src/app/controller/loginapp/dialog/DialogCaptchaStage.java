package app.controller.loginapp.dialog;

import app.utils.StringConstants;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class DialogCaptchaStage {
    private Stage newWindow;
    private DialogCaptchaController controller;

    public DialogCaptchaStage(Stage stage) {
        if (stage != null) {
            Parent root;
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout/dialog_captcha.fxml"));
                root = loader.load();
                Scene secondScene = new Scene(root);
                controller = loader.getController();
                newWindow = new Stage();
                newWindow.setScene(secondScene);
                newWindow.setTitle(StringConstants.TITLE);
                newWindow.initModality(Modality.WINDOW_MODAL);
                newWindow.initOwner(stage);
                newWindow.getScene().getWindow().centerOnScreen();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void show(String message, String urlImage) {
        controller.initData(message, urlImage);
        newWindow.show();
    }

    public void updateCaptcha(String message, String urlImage) {
        controller.initData(message, urlImage);
    }

    public DialogCaptchaController getController() {
        return controller;
    }

    public void hide() {
        controller.showLoading(false);
        newWindow.hide();
    }

    public boolean isShow() {
        return newWindow.isShowing();
    }
}
