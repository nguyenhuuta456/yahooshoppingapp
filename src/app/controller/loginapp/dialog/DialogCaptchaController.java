package app.controller.loginapp.dialog;

import app.App;
import app.utils.Constants;
import app.utils.Utils;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;

public class DialogCaptchaController {
    public Label messageError;
    public ImageView imageCaptcha;
    public TextField inputCaptcha;
    public Button buttonLogin;
    public StackPane loadingView;

    private ICaptchaCallback callback;

    public void initData(String message, String urlImage) {
        showLoading(false);
        inputCaptcha.setText(Constants.EMPTY);
        messageError.setText(message);
        loadImage(urlImage, imageCaptcha);
    }


    public void onLogin(ActionEvent actionEvent) {
        showLoading(true);
        String captcha = inputCaptcha.getText();
        if (callback != null) {
            callback.onLoginWithCaptcha(captcha);
        }
    }

    public interface ICaptchaCallback {
        void onLoginWithCaptcha(String captcha);
    }

    private void loadImage(String url, ImageView imageView) {
        if (url != null && !url.isEmpty()) {
            Image image = new Image(url, 292, 82, true, true, true);
            imageView.setImage(image);
            imageView.setCache(true);
        }
    }


    public void showLoading(boolean isShow) {
        Utils.runOnUIThread(() -> loadingView.setVisible(isShow));
    }

    public void setCallback(ICaptchaCallback callback) {
        this.callback = callback;
    }
}
