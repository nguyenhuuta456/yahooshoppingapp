package app.controller;

import app.utils.Logger;
import app.utils.StringConstants;

public abstract class BaseViewModel<T extends IBaseViewModelCallback> {

    private T baseCallback;

    public BaseViewModel(T callback) {
        this.baseCallback = callback;
    }

    protected T getBaseCallback() {
        return baseCallback;
    }

    protected void print(String message) {
        Logger.d("BaseViewModel", message);
    }

    protected void print(String tag, String message) {
        Logger.d(tag, message);
    }


    public StringConstants getString() {
        return StringConstants.getInstance();
    }

}
