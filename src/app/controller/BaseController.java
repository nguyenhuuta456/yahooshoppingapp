package app.controller;

import app.utils.Logger;
import app.utils.StringConstants;
import app.utils.dialog.DialogApp;
import javafx.fxml.FXML;

public abstract class BaseController<B extends BaseViewModel> {
    private B baseViewModel;

    @FXML
    public void initialize() {
        baseViewModel = createBaseViewModel();
        initView();
        initAction();
        initData();
    }

    protected abstract String TAG();

    protected abstract B createBaseViewModel();

    protected abstract void initView();

    protected abstract void initAction();

    protected abstract void initData();

    protected B getViewModel() {
        return baseViewModel;
    }

    private DialogApp dialogApp;

    protected void showDialog(String message) {
        if (dialogApp == null) {
            dialogApp = new DialogApp();
        }
        dialogApp.initData(message);
        dialogApp.show();
    }

    public StringConstants getString() {
        return StringConstants.getInstance();
    }

    public void print(String message) {
        Logger.d(TAG(), message);
    }
}
