package app.controller.main;

import app.App;
import app.controller.BaseController;
import app.controller.main.dialog.DialogSendLog;
import app.controller.main.loginyahoo.LoginYahooViewModel;
import app.controller.main.manage.ManagerProductController;
import app.controller.main.transaction.TransactionController;
import app.entity.Preference;
import app.entity.ProductUpload;
import app.requests.IBaseCallback;
import app.requests.YahooShoppingModel;
import app.requests.response.CheckVersionResponse;
import app.stage.LoginAppStage;
import app.task.uploadtask.BackgroundServices;
import app.task.uploadtask.IUploadTaskCallback;
import app.utils.Constants;
import app.utils.Logger;
import app.utils.Utils;
import app.utils.dialog.CheckVersionDialog;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v1.DbxEntry;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.*;
import javafx.concurrent.Worker;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import netscape.javascript.JSObject;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;
import org.yaml.snakeyaml.scanner.Constant;

import javax.rmi.CORBA.Util;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import static com.gargoylesoftware.htmlunit.javascript.host.canvas.WebGLRenderingContext.BUFFER_SIZE;

public class MainController extends BaseController<MainViewModel> implements IMainViewModelCallback, IUploadTaskCallback, IJavascript, App.IListenerStopApp {
    private static final String TAG = "MainController";
    private static final int MAX_LOG = 1000;
    public ImageView imageLogout;
    public TabPane tabParent;
    public Tab loginTab;
    public Tab transactionTab;
    public Label version;
    public TextField inputAccount;
    public PasswordField inputPassword;
    public Button buttonLogin;
    public WebView webLog;


    private WebEngine webEngine;
    public Label loginWith;
    public StackPane layoutLogin;
    public AnchorPane layoutLog;

    public StackPane progressBar;
    private LoginYahooViewModel loginYahooModel;
    private BackgroundServices backgroundServices;

    private int countLog = 0;
//    private WebDriver webDriver;
    /*
     * hoangthang2601/ThangHX1984
     *
     * vcmmakzx3ktjcm/ninose110
     * */

    @FXML
    public TransactionController transactionController;

    @FXML
    public ManagerProductController managerProductController;


    @Override
    protected String TAG() {
        return TAG;
    }

    @Override
    protected MainViewModel createBaseViewModel() {
        return new MainViewModel(this);
    }


    @Override
    public void initialize() {
        super.initialize();
        disableTabTransaction(true);
    }

    @Override
    protected void initView() {
        version.setText("version " + Constants.APP_VERSION_CODE);
        webLog.setContextMenuEnabled(false);
        webEngine = webLog.getEngine();
        webEngine.getLoadWorker().stateProperty().addListener((observable, oldValue, newValue) -> {
            print("#webEngine success");
            if (Worker.State.SUCCEEDED.equals(newValue)) {
                JSObject window = (JSObject) webEngine.executeScript("window");
                window.setMember("jsInterface", MainController.this);
            }
        });
        webEngine.load(getClass().getResource("log_panel.html").toExternalForm());
    }

    @Override
    protected void initAction() {
        App.getInstance().registerListenerStopApp(this);
        imageLogout.addEventHandler(javafx.scene.input.MouseEvent.MOUSE_CLICKED, event -> {
            disableTabTransaction(true);
            clearData();
            new LoginAppStage(App.getInstance().getStageApp());
        });
        tabParent.getSelectionModel().selectedItemProperty().addListener((ov, tabOld, tabNew) -> {
                    version.setVisible(tabNew == loginTab);
                }
        );
    }

    @Override
    protected void initData() {
        loginYahooModel = new LoginYahooViewModel(this);
        backgroundServices = BackgroundServices.getInstance();
        backgroundServices.setCallback(this);
        backgroundServices.setShoppingModel(new YahooShoppingModel());
        backgroundServices.setCurrentWebDriver(loginYahooModel.getWebDriver());
        checkVersionApp();
        checkLoginYahoo();
    }

    private void checkVersionApp() {
        YahooShoppingModel model = new YahooShoppingModel();
        print("version app " + Constants.APP_VERSION_CODE);
        model.checkVersionApp(new IBaseCallback<CheckVersionResponse>() {
            @Override
            public void onResponse(CheckVersionResponse response) {
                if (response.getData() == null) return;
                print("version app = " + response.getData().getVersionCode());
                CheckVersionDialog dialog = new CheckVersionDialog(App.getInstance().getStageApp());
                dialog.show();
            }

            @Override
            public void onError(String message) {

            }
        });
    }


    private void clearData() {
        loginYahooModel.logoutApp();
        transactionController.onStopApp();
        managerProductController.onStopApp();
        backgroundServices.stopCountDown();
        backgroundServices.onLogout();
        App.getInstance().savePreference(null);
        App.getInstance().saveCookieToFile(null);
        App.getInstance().setPreference(null);
        File file = new File("logs/");
        try {
            FileUtils.deleteDirectory(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void disableTabTransaction(boolean isDisable) {
        for (Tab tab : tabParent.getTabs()) {
            int index = tabParent.getTabs().indexOf(tab);
            if (index != 0) {
                tab.setDisable(isDisable);
            }
        }
    }


    /*FOR LOGIN YAHOO*/

    private void checkLoginYahoo() {
        boolean hasAccountYahoo = loginYahooModel.hasAccountYahoo();
        if (hasAccountYahoo) {
            inputAccount.setText(loginYahooModel.getUserName());
            inputPassword.setText(loginYahooModel.getPassword());
            loginYahooModel.loginYahoo(loginYahooModel.getUserName(), loginYahooModel.getPassword());
        } else {
            showFormLogin();
        }
    }

    public void onActionLogin() {
        String userName = inputAccount.getText();
        String password = inputPassword.getText();
        loginYahooModel.loginYahoo(userName, password);
    }

    public void onEnter() {
        buttonLogin.fire();
    }

    @Override
    public void progressLoading(boolean isShow) {
        buttonLogin.setDisable(isShow);
        progressBar.setVisible(isShow);
    }

    //onLogin yahoo success
    @Override
    public void onSuccess() {
        print("#LOGIN SUCCESS");
        layoutLogin.setVisible(false);
        layoutLog.setVisible(true);
        loginWith.setVisible(true);
        String userName = loginYahooModel.getUserName();
        loginWith.setText(userName + "と接続しています");
        disableTabTransaction(false);
        Preference preference = App.getInstance().getPreference();
        backgroundServices.initData(preference.getHost(), preference.getAccountId());
        Utils.postDelay(() -> {
            transactionController.createNewWebDriver();
            managerProductController.createNewWebDriver();
        }, 1000);

    }

    @Override
    public void showMessage(String message) {
        showDialog(message);
    }

    private void showFormLogin() {
        layoutLogin.setVisible(true);
        layoutLog.setVisible(false);
        loginWith.setVisible(false);
    }
    /*FOR WEB LOG*/


    private String getCurrentTime() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return formatter.format(new Date());
    }

    @Override
    public void statusWorking(String message) {
        if (countLog < MAX_LOG) {
            countLog++;
        }
        String logMessage = "[" + getCurrentTime() + "]: " + message;
        String formattedMessage = logMessage.replaceAll("\\r\\n|\\r|\\n", Constants.SPACE_CHARACTER);
        String script = "appendLog('" + formattedMessage + "');";
        if (countLog == MAX_LOG) {
            String remove = "clearLog();";
            webEngine.executeScript(remove);
        }
        webEngine.executeScript(script);
        if (message != null && message.equals(BackgroundServices.SLEEP_JOB)) {
            stopJob();
        }
    }

    @Override
    public void serviceRunning(boolean isStart) {
        if (isStart) {
            startJob();
        } else {
            stopJob();
        }
    }

    @Override
    public void startJob() {
        print("startJob");
        String scripStart = "disableStartService(true)";
        String scripStop = "disableStopService(false)";
        webEngine.executeScript(scripStart);
        webEngine.executeScript(scripStop);
        backgroundServices.setRunService(true);
        backgroundServices.startServiceUpload();
        backgroundServices.stopCountDown();
    }

    @Override
    public void stopJob() {
        print("stopJob");
        String scripStart = "disableStartService(false)";
        String scripStop = "disableStopService(true)";
        webEngine.executeScript(scripStart);
        webEngine.executeScript(scripStop);
        backgroundServices.setRunService(false);
    }

    @Override
    public void sendLog() {
        onSendLog();
    }

    @Override
    public void openUrl(String url) {

    }

    @Override
    public void onDoneTask(ProductUpload object) {
        if (object.getPublishStatus() == ProductUpload.STATUS_DONE) {
            statusWorking("出品成功でした");
        }
    }

    @Override
    public void onFailure(String message) {
        showDialog(message);
    }

    @Override
    public void onStopApp() {
        loginYahooModel.logoutApp();
        transactionController.onStopApp();
        managerProductController.onStopApp();
    }

    public void onSendLog() {
        DialogSendLog dialogSendLog = new DialogSendLog();
        dialogSendLog.setCallback(this::uploadDropBox);
        dialogSendLog.show();
    }


    private void uploadDropBox(String feedback) {
        print("#Description Error : " + feedback);
        progressLoading(true);
        List<String> listServer = Constants.Server.getListServer();
        new Thread(() -> {
            zipFile();
            Preference preference = App.getInstance().getPreference();
            DbxRequestConfig config = DbxRequestConfig.newBuilder("dropbox/java-tutorial").build();
            DbxClientV2 client = new DbxClientV2(config, Constants.AccessKey);
            try {
                File fileDes = new File(zipFile);
                print("#total " + fileDes.length());
                InputStream in = new FileInputStream(fileDes);
                String host = preference.getHost();
                String splitHost = host.split("//")[1];
                String address = splitHost.split("[:/]")[0];
                String findAddress = listServer.parallelStream().filter(server -> {
                    String[] splitServer = server.split("_");
                    String serverDetail = server;
                    if (splitServer.length > 1) {
                        serverDetail = splitServer[1];
                    }
                    return serverDetail.contains(address);
                }).findFirst().orElse("");
                if (findAddress.isEmpty()) {
                    showDialog("見つからないURL");
                    return;
                }
                String formatFileLog = "/YahooShopping/%s/[%s] - %s_%s_%s.zip";
                String environment = SystemUtils.IS_OS_MAC ? "mac" : "window";
                String versionApp = Constants.APP_VERSION_CODE;
                String fileName = String.format(formatFileLog, findAddress, Utils.convertDate(System.currentTimeMillis(), Utils.formatDate2), preference.getAccountApp(), environment, versionApp);
                client.files().uploadBuilder(fileName).withMode(WriteMode.OVERWRITE).uploadAndFinish(in, bytesWritten -> {
                    print("#currnet " + bytesWritten);
                    if (fileDes.length() <= bytesWritten) {
                        Utils.runOnUIThread(() -> {
                            if (fileDes.delete()) {
                                print(" delete");
                            } else {
                                print("not delete");
                            }
                            progressLoading(false);
                        });
                    }
                });
            } catch (IOException | DbxException e) {
                e.printStackTrace();
            }
        }).start();
    }

    private String zipFile = "LogFile.zip";

    private void zipFile() {
        File file = new File("logs/");
        BufferedInputStream origin = null;
        ZipOutputStream out = null;

        try {
            out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zipFile)));
            byte data[] = new byte[BUFFER_SIZE];
            if (file.isDirectory()) {
                for (File item : Objects.requireNonNull(file.listFiles())) {
                    FileInputStream fi = new FileInputStream(item);
                    origin = new BufferedInputStream(fi, BUFFER_SIZE);
                    try {
                        ZipEntry entry = new ZipEntry(item.getName());
                        out.putNextEntry(entry);
                        int count;
                        while ((count = origin.read(data, 0, BUFFER_SIZE)) != -1) {
                            out.write(data, 0, count);
                        }
                    } finally {
                        origin.close();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
