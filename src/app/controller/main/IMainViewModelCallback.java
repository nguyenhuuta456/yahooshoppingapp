package app.controller.main;

import app.controller.IBaseViewModelCallback;

public interface IMainViewModelCallback extends IBaseViewModelCallback {
    void statusWorking(String message);

    void startJob();
    void stopJob();
    void sendLog();

}
