package app.controller.main.transaction.Cell;

import app.entity.Transaction;
import app.utils.Logger;
import app.utils.Utils;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import java.io.IOException;

public class StatusProductCell extends TableCell<Transaction, Transaction> {
    private static final String TAG = "StatusProductCell";
    private FXMLLoader mLLoader;

    @FXML
    private VBox rootView;
    @FXML
    private ImageView imageStatus;
    @FXML
    private Label statusProduct;

    @Override
    protected void updateItem(Transaction item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null) {
            if (mLLoader == null) {
                mLLoader = new FXMLLoader(getClass().getResource("/layout/cell_status.fxml"));
                mLLoader.setController(this);
                try {
                    mLLoader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            Utils.loadImage(item.getImageStatus(), imageStatus);
            statusProduct.setText(item.getStatusProduct());
            setText(null);
            setGraphic(rootView);
            setAlignment(Pos.CENTER);
        }
    }
}