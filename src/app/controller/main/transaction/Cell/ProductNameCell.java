package app.controller.main.transaction.Cell;

import app.entity.Transaction;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

public class ProductNameCell extends TableCell<Transaction, Transaction> {
    private FXMLLoader mLLoader;

    private AnchorPane rootView;
    private ImageView imageProduct;
    private Label productName;


    @Override
    protected void updateItem(Transaction item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null) {
            if (mLLoader == null) {
                mLLoader = new FXMLLoader(getClass().getResource("/layout/product_name_cell.fxml"));
                mLLoader.setController(this);
                try {
                    mLLoader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            productName.setText(item.getProductName());
            setText(null);
            setGraphic(rootView);
            setAlignment(Pos.CENTER);

        }
    }
}
