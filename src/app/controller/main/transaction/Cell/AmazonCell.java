package app.controller.main.transaction.Cell;

import app.entity.Transaction;
import app.utils.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.layout.StackPane;

import java.io.IOException;

public class AmazonCell extends TableCell<Transaction, Transaction> {
    private static final String TAG = "AmazonCell";
    private FXMLLoader mLLoader;

    @FXML
    private StackPane rootView;
    @FXML
    private Button buttonAmazon;

    @Override
    protected void updateItem(Transaction item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null) {
            if (mLLoader == null) {
                mLLoader = new FXMLLoader(getClass().getResource("/layout/amazon_cell.fxml"));
                mLLoader.setController(this);
                try {
                    mLLoader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            buttonAmazon.setOnAction(event -> Logger.d(TAG, "buttonAmazon click"));
            setText(null);
            setGraphic(rootView);
            setAlignment(Pos.CENTER);
        }
    }
}
