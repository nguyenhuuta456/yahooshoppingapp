package app.controller.main.transaction;

import app.App;
import app.controller.BaseController;
import app.controller.BaseViewModel;
import app.controller.main.IJavascript;
import app.entity.Preference;
import app.requests.IBaseCallback;
import app.requests.YahooShoppingModel;
import app.requests.params.GetAsinParams;
import app.requests.response.ListAsinResponse;
import app.utils.Constants;
import app.utils.Utils;
import javafx.concurrent.Worker;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import netscape.javascript.JSObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.openqa.selenium.Cookie;

import java.io.IOException;
import java.net.URI;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TransactionController extends BaseController implements App.IListenerStopApp, IJavascript {
    private static final String PAGE_TRANSACTION = "https://creator.shopping.yahoo.co.jp/%s/order/history-list/";

    public StackPane loadingView;
    public WebView webViewProduct;


    private Map<String, List<String>> mapOrder;
    private WebEngine webEngine;
    private YahooShoppingModel shoppingModel;

    @Override
    protected String TAG() {
        return TransactionController.class.getSimpleName();
    }

    @Override
    protected BaseViewModel createBaseViewModel() {
        return null;
    }

    @Override
    protected void initView() {
        webEngine = webViewProduct.getEngine();
    }

    @Override
    protected void initAction() {
        App.getInstance().registerListenerStopApp(this);
        webEngine.getLoadWorker().stateProperty().addListener((ov, oldState, newState) -> {
            String url = webEngine.getLocation();
            showLoading(true);
            if (Worker.State.SUCCEEDED.equals(newState)) {
                print("#SUCCEEDED " + url);
                showLoading(false);
                JSObject window = (JSObject) webEngine.executeScript("window");
                window.setMember("jsInterface", TransactionController.this);
                String regex = "OrderID=(?<orderId>.*)";
                Pattern pattern = Pattern.compile(regex);
                Matcher matcher = pattern.matcher(url);
                String orderId = Constants.EMPTY;
                while (matcher.find()) {
                    orderId = matcher.group("orderId");
                }
                print("#orderNumberId = " + orderId);
                if (!orderId.isEmpty()) {
                    if (mapOrder.containsKey(orderId)) {
                        addButtonAmazon(mapOrder.get(orderId));
                    } else {
                        String html = (String) webEngine.executeScript("document.documentElement.outerHTML");
                        Document document = Jsoup.parse(html);
                        Elements tagCodes = document.getElementsByClass("elCode");
                        if (tagCodes != null && tagCodes.size() > 0) {
                            List<String> listCode = new ArrayList<>();
                            tagCodes.forEach(elementCode -> {
                                String itemCode = elementCode.text();
                                if (itemCode != null && !itemCode.isEmpty()) {
                                    itemCode = itemCode.replace("（商品コード）", "").trim();
                                    print("itemCode " + itemCode);
                                    listCode.add(itemCode);
                                }
                            });
                            if (listCode.size() > 0) {
                                getAsinByItemCode(listCode, orderId);
                            }
                        } else {
                            print("#tagCodes null");
                        }
                    }
                }
            }

        });
    }

    private void showLoading(boolean visible) {
        loadingView.setVisible(visible);
    }

    @Override
    protected void initData() {
        mapOrder = new HashMap<>();
    }


    private Preference preference;

    public void createNewWebDriver() {
        sendCookies();
        shoppingModel = new YahooShoppingModel();
        preference = App.getInstance().getPreference();
        if (preference != null) {
            String shipId = preference.getShopId();
            String currentUrl = String.format(PAGE_TRANSACTION, shipId);
            webEngine.load(currentUrl);
        }
    }

    public void reloadData() {
        if (!loadingView.isVisible()) {
            webEngine.reload();
        }
    }

    private void sendCookies() {
        URI uri = URI.create("https://creator.shopping.yahoo.co.jp");
        Map<String, List<String>> headers = new LinkedHashMap<>();
        Set<Cookie> cookies = App.getInstance().getCookieFromFile();
        List<String> stringCookies = new ArrayList<>();
        for (Cookie item : cookies) {
            stringCookies.add(item.toString());
        }
        headers.put("Set-Cookie", stringCookies);
        try {
            java.net.CookieHandler.getDefault().put(uri, headers);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addButtonAmazon(List<String> listAsin) {
        listAsin.forEach(asin -> {
            int index = listAsin.indexOf(asin);
            String url = "https://www.amazon.co.jp/dp/" + asin;
            String scrip = "    function insertButtonAmazon() {\n" +
                    "        var tagCode = document.getElementsByClassName('elCode')[" + index + "];" +
                    "        var para = document.createElement('p');\n" +
                    "        para.innerHTML = '</br> <a style =\"font-size: 16px\" href=\"#\" onclick=\"jsInterface.openUrl(\\'" + url + "\\')\" target=\"_blank\">アマゾンへ</a>';" +
                    "        tagCode.appendChild(para);\n" +
                    "    }" +
                    "\n" +
                    "insertButtonAmazon();";
            webEngine.executeScript(scrip);
        });
    }

    private void getAsinByItemCode(List<String> list, String orderId) {
        GetAsinParams params = new GetAsinParams(preference.getAccountId(), list);
        shoppingModel.getAsinsByItemCode(params, new IBaseCallback<ListAsinResponse>() {
            @Override
            public void onResponse(ListAsinResponse response) {
                if (response != null && response.isSuccess()) {
                    List<String> listAsin = response.getData();
                    print("#getAsinByItemCode listAsin : " + listAsin);
                    if (listAsin != null && listAsin.size() > 0) {
                        mapOrder.put(orderId, listAsin);
                        addButtonAmazon(listAsin);
                    } else {
                        print("#getAsinByItemCode listAsin empty");
                    }
                } else {
                    print("#getAsinByItemCode : " + response.getMessage());
                }
            }

            @Override
            public void onError(String message) {
                print("#getAsinByItemCode onError : " + message);
            }
        });
    }


    @Override
    public void onStopApp() {

    }

    @Override
    public void openUrl(String url) {
        print("url " + url);
        Utils.openBrowser(url);
    }
}
