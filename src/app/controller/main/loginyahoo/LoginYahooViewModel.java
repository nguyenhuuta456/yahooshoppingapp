package app.controller.main.loginyahoo;

import app.App;
import app.controller.BaseViewModel;
import app.controller.IBaseViewModelCallback;
import app.controller.loginapp.dialog.DialogCaptchaController;
import app.controller.loginapp.dialog.DialogCaptchaStage;
import app.entity.Preference;
import app.requests.BaseResponse;
import app.requests.IBaseCallback;
import app.requests.YahooShoppingModel;
import app.requests.params.UpdateAccountParams;
import app.task.logintask.ILoginTaskCallback;
import app.task.logintask.LoginYahooTask;
import app.utils.Constants;
import app.utils.Utils;
import app.utils.WebDriverUtils;
import com.google.gson.Gson;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class LoginYahooViewModel extends BaseViewModel<IBaseViewModelCallback> implements ILoginTaskCallback {
    private static final String TAG = "LoginYahooViewModel";

    private YahooShoppingModel yahooShoppingModel;
    private ExecutorService loginService;
    private Preference preference;
    private WebDriver webDriver;
    private LoginYahooTask loginTask;
    private boolean hasAccount;

    public LoginYahooViewModel(IBaseViewModelCallback callback) {
        super(callback);
        preference = App.getInstance().getPreference();
        yahooShoppingModel = new YahooShoppingModel();
        loginService = Executors.newSingleThreadExecutor();
        this.webDriver = WebDriverUtils.getInstance().createWebDriver();
        hasAccount = preference.getAccountYahoo() != null && preference.getPasswordYahoo() != null;
        loginTask = new LoginYahooTask(webDriver, this);
    }

    public boolean hasAccountYahoo() {
        return hasAccount;
    }

    public void loginYahoo(String userName, String password) {
        String message = Constants.EMPTY;
        if (userName == null || userName.isEmpty()) {
            message = getString().user_name_not_null;
        } else if (password == null || password.isEmpty()) {
            message = getString().password_not_null;
        }
        if (message.isEmpty()) {
            print(TAG, "#loginYahoo");
            getBaseCallback().progressLoading(true);
            Set<Cookie> cookies = App.getInstance().getCookieFromFile();
            if (cookies != null && !cookies.isEmpty()) {
                loginYahooWithCookie(cookies);
            } else {
                preference.setAccountYahoo(userName);
                preference.setPasswordYahoo(password);
                loginTask.setAccount(preference);
                loginService.execute(loginTask);
            }
        } else {
            getBaseCallback().showMessage(message);
        }
    }

    private void loginYahooWithCookie(Set<Cookie> cookies) {
        print(TAG, "#loginYahooWithCookie");
        getBaseCallback().progressLoading(true);
        loginService.execute(() -> {
            String url = preference.getUrlLoginYahoo() == null ? LoginYahooTask.URL_LOGIN_YAHOO : preference.getUrlLoginYahoo();
            webDriver.get(url);
            for (Cookie cookie : cookies) {
                if (cookie != null) {
                    webDriver.manage().addCookie(cookie);
                }
            }
            webDriver.get(url);
            //TODO handle session expired
            Utils.runOnUIThread(() -> {
                App.getInstance().saveCookieToFile(webDriver.manage().getCookies());
                saveCookie();
                getBaseCallback().progressLoading(false);
                getBaseCallback().onSuccess();
            });
        });
    }

    public String getUserName() {
        return preference.getAccountYahoo();
    }

    public String getPassword() {
        return preference.getPasswordYahoo();
    }

    public void logoutApp() {
        hasAccount = false;
        if (webDriver != null) {
            try {
                webDriver.manage().deleteAllCookies();
                webDriver.quit();
            } catch (WebDriverException e) {
                e.printStackTrace();
            } finally {
                webDriver = null;
            }
        }
    }

    @Override
    public void onDoneTask(Preference preference) {
        print(TAG, "#finish login yahoo");
        if (dialogCaptcha != null) {
            dialogCaptcha.hide();
        }
        Set<Cookie> cookies = webDriver.manage().getCookies();
        App.getInstance().saveCookieToFile(cookies);
        saveCookie();
        if (!hasAccount) {
            hasAccount = true;
            updateAccount(preference, 1);
        } else {
            App.getInstance().savePreference(preference);
            getBaseCallback().progressLoading(false);
            getBaseCallback().onSuccess();
        }
    }

    private void saveCookie() {
        Set<Cookie> cookies = webDriver.manage().getCookies();
        Gson gson = new Gson();
        String json = gson.toJson(cookies);
        yahooShoppingModel.saveCookie(json, new IBaseCallback<BaseResponse>() {
            @Override
            public void onResponse(BaseResponse response) {
                print("#Save cookie onResponse " + response.isSuccess());
            }

            @Override
            public void onError(String message) {
                print("#Save cookie onError " + message);
            }
        });
    }

    private void updateAccount(Preference preference, int countRetry) {
        UpdateAccountParams params = new UpdateAccountParams(preference.getAccountId(), preference.getAccountYahoo(), preference.getPasswordYahoo(), preference.getShopId());
        print(TAG, "#update account - " + params.toString());
        yahooShoppingModel.updateAccount(preference.getHost(), params, new IBaseCallback<BaseResponse>() {
            @Override
            public void onResponse(BaseResponse response) {
                getBaseCallback().progressLoading(false);
                if (response.isSuccess()) {
                    print("updateAccount success");
                    getBaseCallback().onSuccess();
                    App.getInstance().savePreference(preference);
                } else {
                    print("updateAccount failure " + response.getMessage());
                    getBaseCallback().showMessage(response.getMessage());
                }
            }

            @Override
            public void onError(String message) {
                print("updateAccount onError " + message);
                if (countRetry <= 3) {
                    updateAccount(preference, countRetry + 1);
                } else {
                    getBaseCallback().progressLoading(false);
                    getBaseCallback().showMessage(message);
                }
            }
        });
    }

    @Override
    public void onFailure(String message) {
        getBaseCallback().progressLoading(false);
        getBaseCallback().showMessage(message);
    }

    @Override
    public void hindLoading() {
        getBaseCallback().progressLoading(false);
    }

    private DialogCaptchaStage dialogCaptcha;

    @Override
    public void showDialogCaptcha(String message, String urlImage, DialogCaptchaController.ICaptchaCallback captchaCallback) {
        if (dialogCaptcha == null) {
            dialogCaptcha = new DialogCaptchaStage(App.getInstance().getStageApp());
        }
        dialogCaptcha.getController().setCallback(captchaCallback);
        if (dialogCaptcha.isShow()) {
            dialogCaptcha.updateCaptcha(message, urlImage);
        } else {
            dialogCaptcha.show(message, urlImage);
        }
    }

    public WebDriver getWebDriver() {
        return webDriver;
    }
}
