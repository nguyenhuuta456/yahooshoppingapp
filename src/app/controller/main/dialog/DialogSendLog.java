package app.controller.main.dialog;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.stage.Modality;

import java.io.IOException;

public class DialogSendLog extends Dialog {
    private Button buttonOk, buttonClose;
    IYahooDialogCallback callback;
    DialogLogController controller;

    public DialogSendLog() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout/layout_dialog_send_log.fxml"));
            initModality(Modality.APPLICATION_MODAL);
            Parent root = loader.load();
            controller = loader.getController();
            setTitle("Notification");
            getDialogPane().setContent(root);
            getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CLOSE);
            buttonOk = (Button) getDialogPane().lookupButton(ButtonType.OK);
            buttonOk.setText("はい");
            buttonOk.addEventFilter(ActionEvent.ACTION, event -> {
                if (callback != null) {
                    String feedback = controller.inputFeedback.getText().trim();
                    callback.actionButtonOK(feedback);
                }
                close();
            });

            buttonClose = (Button) getDialogPane().lookupButton(ButtonType.CLOSE);
            buttonClose.setText("キャンセル");
            buttonClose.addEventFilter(ActionEvent.ACTION, event -> {
                close();
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setCallback(IYahooDialogCallback callback) {
        this.callback = callback;
    }

    public interface IYahooDialogCallback {
        void actionButtonOK(String feedback);

    }

}

