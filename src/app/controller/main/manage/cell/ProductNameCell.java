package app.controller.main.manage.cell;

import app.entity.ProductManage;
import app.utils.Utils;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

import java.io.IOException;

public class ProductNameCell extends TableCell<ProductManage, ProductManage> {
    private FXMLLoader mLLoader;
    @FXML
    private HBox rootView;
    @FXML
    private ImageView imageProduct;
    @FXML
    private Label productName;


    @Override
    protected void updateItem(ProductManage item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null) {
            if (mLLoader == null) {
                mLLoader = new FXMLLoader(getClass().getResource("/layout/product_manage_name_cell.fxml"));
                mLLoader.setController(this);
                try {
                    mLLoader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            Utils.loadImage(item.getMainImage(), imageProduct);
            productName.setText(item.getName());
            setText(null);
            setGraphic(rootView);
            setAlignment(Pos.CENTER);
        }
    }
}
