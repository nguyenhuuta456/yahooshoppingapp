package app.controller.main.manage.cell;

import app.entity.ProductManage;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TableCell;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;

import java.io.IOException;

public class DeleteCell extends TableCell<String, String> {
    private FXMLLoader mLLoader;
    @FXML
    private StackPane rootView;
    @FXML
    private ImageView imageDelete;

    private IDeleteProduct callback;

    public DeleteCell(IDeleteProduct callback) {
        this.callback = callback;
    }

    @Override
    protected void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null) {
            if (mLLoader == null) {
                mLLoader = new FXMLLoader(getClass().getResource("/layout/delete_cell.fxml"));
                mLLoader.setController(this);
                try {
                    mLLoader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            imageDelete.setOnMouseClicked(event -> callback.onDeleteProduct(item, getIndex()));
            setText(null);
            setGraphic(rootView);
        }
    }

    public interface IDeleteProduct {
        void onDeleteProduct(String javascript, int position);
    }
}
