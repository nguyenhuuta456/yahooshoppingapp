package app.controller.main.manage;

import app.App;
import app.controller.BaseController;
import app.controller.BaseViewModel;
import app.entity.Preference;
import app.entity.ProductManage;
import app.requests.BaseResponse;
import app.requests.IBaseCallback;
import app.requests.YahooShoppingModel;
import app.task.managetask.DeletePageTask;
import app.task.managetask.expiredtask.DeleteExpiredTask;
import app.task.managetask.stocktask.DeleteTask;
import app.task.managetask.stocktask.IDeleteTask;
import app.task.managetask.IDeletePageTask;
import app.utils.Constants;
import app.utils.Utils;
import app.utils.WebDriverUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Worker;
import javafx.scene.control.*;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import netscape.javascript.JSObject;
import org.openqa.selenium.WebDriver;
import org.w3c.dom.Element;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ManagerProductController extends BaseController implements App.IListenerStopApp, IDeletePageTask {
    public Button buttonRemoveStock;
    public Button buttonRemoveExpired;
    public Button buttonRemoveSelected;
    public StackPane loadingView;
    public ProgressIndicator progressDelete;
    public WebView webView;
    private WebEngine webEngine;


    private ExecutorService executorService;
    private DeleteTask deleteTask;
    private DeleteExpiredTask deleteExpiredTask;
    private YahooShoppingModel shoppingModel;

    private ObservableList<ProductManage> observableList = FXCollections.observableArrayList();
    private String firstPage = Constants.EMPTY;
    private String itemCode;

    @Override
    protected String TAG() {
        return ManagerProductController.class.getSimpleName();
    }

    @Override
    protected BaseViewModel createBaseViewModel() {
        return null;
    }

    @Override
    protected void initView() {
        webEngine = webView.getEngine();
//        initTableView();
    }

    @Override
    protected void initAction() {
        App.getInstance().registerListenerStopApp(this);
        buttonRemoveStock.setOnAction(event -> createWebDriverDelete());
        buttonRemoveExpired.setOnAction(event -> createWebDriverDeleteExpired());
        buttonRemoveSelected.setOnAction(event -> getItemSelected());
        webEngine.setConfirmHandler(this::showConfirm);
        webEngine.getLoadWorker().stateProperty().addListener((ov, oldState, newState) -> {
            String url = webEngine.getLocation();
            showLoading(true);
            if (Worker.State.SUCCEEDED.equals(newState)) {
                print("#SUCCEEDED " + url);
                showLoading(false);
                if (url.contains(firstPage)) {
                    JSObject window = (JSObject) webEngine.executeScript("window");
                    window.setMember("app", ManagerProductController.this);
                    injectFunctionToWebView();
                }
            }

        });

    }

    @Override
    protected void initData() {

    }

    @Override
    public void deleteItem(String itemCode, String id) {
        print(itemCode + "/" + id);
        if (itemCode == null) {
            webEngine.reload();
            return;
        }
        if (id != null) {
            this.itemCode = itemCode;
            String scriptDelete = "deleteItem('" + itemCode + "', '" + id + "')";
            print(scriptDelete);
            webEngine.executeScript(scriptDelete);
        } else {
            deleteRow(itemCode);
            requestDeleteProduct(itemCode);
        }
    }

    private synchronized void deleteRow(String rowId) {
        String js = "window.deleteRow = function (rowId) {\n" +
                "        var row = document.getElementById(rowId);\n" +
                "        if (row != null) {\n" +
                "            row.innerHTML = '';\n" +
                "        }\n" +
                "    }";
        webEngine.executeScript(js);
        webEngine.executeScript("deleteRow('" + rowId + "')");
    }

    private void injectFunctionToWebView() {
        String injectFunction = "window.injectFunctionToWebView = function () {\n" +
                "    var dvCategory = document.getElementsByClassName(\"dvCategory\");\n" +
                "        if (dvCategory != null && dvCategory.length > 0) {\n" +
                "            var header = dvCategory[0].getElementsByClassName(\"elTotop\")[0];\n" +
                "            header.innerHTML = '<input id=\"headerCheckbox\" onclick=\"checkChangeCheckBox(0);\" type=\"checkbox\"/>';\n" +
                "        }\n" +
                "        var allItems = document.getElementById(\"allItem\");\n" +
                "        if (allItems != null) {\n" +
                "            var listButtonDelete = allItems.getElementsByClassName(\"elEdit\");\n" +
                "            if (listButtonDelete != null && listButtonDelete.length > 0) {\n" +
                "                for (var position = 0; position < listButtonDelete.length; position++) {\n" +
                "                    var checkBoxTag = allItems.getElementsByClassName(\"elTotop\")[position];\n" +
                "                    checkBoxTag.innerHTML = '';\n" +
                "                    var liTag = listButtonDelete[position];\n" +
                "                    var tagDelete = liTag.getElementsByTagName(\"a\")[1];\n" +
                "                    var functionOnclick = tagDelete.getAttribute(\"onclick\");\n" +
                "                    var funcDelete = \"app.\" + functionOnclick;\n" +
                "                    tagDelete.setAttribute(\"onclick\", funcDelete);\n" +
                "                    var checkbox = document.createElement(\"input\");\n" +
                "                    checkbox.type = \"checkbox\";\n" +
                "                    checkbox.className = \"checkbox\";\n" +
                "                    checkbox.setAttribute(\"onclick\", \"checkChangeCheckBox(1)\");\n" +
                "                    checkbox.value = functionOnclick;\n" +
                "                    checkBoxTag.appendChild(checkbox);\n" +
                "                }\n" +
                "            }\n" +
                "        }" +
                "    }";

        String jsCheckChange = "window.checkChangeCheckBox = function (type) {\n" +
                "        var headerCheckBox = document.getElementById(\"headerCheckbox\");\n" +
                "        var listCheck = document.getElementsByClassName(\"checkbox\");\n" +
                "        if (type === 0) {\n" +
                "            var hasCheck = headerCheckBox.checked;\n" +
                "            for (var position = 0; position < listCheck.length; position++) {\n" +
                "                var inputCheckBox = listCheck[position];\n" +
                "                inputCheckBox.checked = hasCheck;\n" +
                "            }\n" +
                "        } else {\n" +
                "            var allCheck = true;\n" +
                "            for (var index = 0; index < listCheck.length; index++) {\n" +
                "                var inputCheckBox1 = listCheck[index];\n" +
                "                if (!inputCheckBox1.checked) {\n" +
                "                    allCheck = false;\n" +
                "                    break;\n" +
                "                }\n" +
                "            }\n" +
                "            headerCheckBox.checked = allCheck;\n" +
                "        }" +
                "    }";
        webEngine.executeScript(jsCheckChange);
        webEngine.executeScript(injectFunction);
        webEngine.executeScript("injectFunctionToWebView()");
    }

    private boolean showConfirm(String message) {
        print("showConfirm");
        Dialog<ButtonType> confirm = new Dialog<>();
        confirm.getDialogPane().setContentText(message);
        confirm.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        boolean result = confirm.showAndWait().filter(ButtonType.OK::equals).isPresent();
        if (result) {
            showLoading(true);
            checkDeleteSuccess();
            requestDeleteProduct(itemCode);
        } else {
            itemCode = "";
        }
        return result;
    }

    private void getItemSelected() {
        String jsGetChecked = "function getItemSelected() {\n" +
                "        var listCheck = document.getElementsByClassName(\"checkbox\");\n" +
                "        var listJavaScrip = Array();\n" +
                "        for (var index = 0; index < listCheck.length; index++) {\n" +
                "            var inputCheckBox = listCheck[index];\n" +
                "            if (inputCheckBox.checked) {\n" +
                "                var script = '\"' + inputCheckBox.getAttribute(\"value\") + '\"';\n" +
                "                listJavaScrip.push(script);\n" +
                "            }\n" +
                "        }\n" +
                "        return listJavaScrip;\n" +
                "    }\n" +
                "getItemSelected();";
        Object object = webEngine.executeScript(jsGetChecked);
        String array = "[" + object.toString() + "]";
        Gson gson = new Gson();
        Type type = new TypeToken<List<String>>() {
        }.getType();
        List<String> list = gson.fromJson(array, type);
        if (list != null && list.size() > 0) {
            showLoading(true);
            if (deletePageTask == null) {
                WebDriver webDriver = WebDriverUtils.getInstance().createWebDriver();
                deletePageTask = new DeletePageTask(webDriver, this);
            }
            deletePageTask.setCurrentUrl(webEngine.getLocation());
            deletePageTask.setListJavaScrip(list);
            executorService.execute(deletePageTask);
        } else {
            print("No Item Selected");
        }
    }

    private DeletePageTask deletePageTask;


    private void checkDeleteSuccess() {
        String productId = "it" + itemCode;
        Utils.postDelay(() -> {
            Element itemId = webEngine.getDocument().getElementById(productId);
            print("item has delete " + itemId);
            if (itemId != null) {
                checkDeleteSuccess();
            } else {
                showLoading(false);
            }
        }, 2000);
    }

    private void requestDeleteProduct(String itemCode) {
        print("#requestDeleteProduct " + itemCode);
        List<String> list = new ArrayList<>();
        list.add(itemCode);
        showLoading(true);
        shoppingModel.deleteProduct(list, new IBaseCallback<BaseResponse>() {
            @Override
            public void onResponse(BaseResponse response) {
                print("#requestDeleteProduct " + response.isSuccess());
            }

            @Override
            public void onError(String message) {
                print("#requestDeleteProduct error" + message);
            }
        });

    }

    public void createNewWebDriver() {
        shoppingModel = new YahooShoppingModel();
        executorService = Executors.newCachedThreadPool();
        Preference preference = App.getInstance().getPreference();
        if (preference != null) {
            firstPage = String.format("https://creator.shopping.yahoo.co.jp/%s/item/list/", preference.getShopId());
            webEngine.load(firstPage);
        }
    }

    private void createWebDriverDelete() {
        buttonRemoveStock.setDisable(true);
        progressDelete.setVisible(true);
        if (deleteTask == null) {
            WebDriver webDriver = WebDriverUtils.getInstance().createWebDriver();
            deleteTask = new DeleteTask(webDriver, new IDeleteTask() {
                @Override
                public void deleteItem(String rowId) {
                    deleteRow(rowId);
                }

                @Override
                public void onDoneTask(Object object) {
                    print("#onDoneTask Stock");
                    buttonRemoveStock.setDisable(false);
                    showProgressDelete();
                }

                @Override
                public void onFailure(String message) {

                }
            });
        }
        executorService.execute(deleteTask);
    }

    private void createWebDriverDeleteExpired() {
        buttonRemoveExpired.setDisable(true);
        progressDelete.setVisible(true);
        if (deleteExpiredTask == null) {
            WebDriver webDriver = WebDriverUtils.getInstance().createWebDriver();
            deleteExpiredTask = new DeleteExpiredTask(webDriver, new IDeleteTask() {
                @Override
                public void deleteItem(String itemId) {
                    deleteRow(itemId);
                }

                @Override
                public void onDoneTask(Object object) {
                    print("#onDoneTask Expired");
                    buttonRemoveExpired.setDisable(false);
                    showProgressDelete();
                }

                @Override
                public void onFailure(String message) {

                }
            });
        }
        executorService.execute(deleteExpiredTask);
    }

    private synchronized void showProgressDelete() {
        if (!buttonRemoveExpired.isDisable() && !buttonRemoveStock.isDisable()) {
            progressDelete.setVisible(false);
        }
    }


    @Override
    public void showLoading(boolean isShow) {
        loadingView.setVisible(isShow);
    }

    @Override
    public void onDoneTask(List<ProductManage> object) {
        showLoading(false);
        if (object != null) {
            observableList.setAll(object);
            webEngine.reload();
        }
    }


    @Override
    public void onFailure(String message) {

    }

    @Override
    public void onStopApp() {
        if (executorService != null) {
            executorService.shutdownNow();
        }
        if (deleteTask != null) {
            deleteTask.onStopApp();
        }
        if (deleteExpiredTask != null) {
            deleteExpiredTask.onStopApp();
        }
    }

}
