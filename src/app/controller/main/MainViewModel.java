package app.controller.main;

import app.App;
import app.controller.BaseViewModel;
import app.entity.Preference;

public class MainViewModel extends BaseViewModel<IMainViewModelCallback> {
    private Preference preference;

    public MainViewModel(IMainViewModelCallback callback) {
        super(callback);
        preference = App.getInstance().getPreference();
    }


    boolean isLogin() {
        return preference.getAccountYahoo() != null;
    }
}
