package app.controller;

public interface IBaseViewModelCallback {
    void progressLoading(boolean isShow);

    void onSuccess();

    void showMessage(String message);
}
