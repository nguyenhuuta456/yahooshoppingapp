package app.task;

import app.utils.Logger;
import javafx.scene.web.WebEngine;
import org.openqa.selenium.WebDriver;

public abstract class BaseTask<I extends IBaseTaskCallback> extends BaseWebViewModel implements Runnable {
    private I baseTaskCallback;

    public BaseTask(WebDriver webDriver, I callback) {
        super(webDriver);
        this.baseTaskCallback = callback;
    }

    @Override
    public void run() {

    }

    public I getCallback() {
        return baseTaskCallback;
    }

    public void print(String tag, String message) {
        Logger.d(tag, message);
    }
}
