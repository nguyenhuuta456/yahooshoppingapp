package app.task.uploadtask;

import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import app.entity.ProductUpload;
import app.requests.BaseResponse;
import app.requests.IBaseCallback;
import app.requests.YahooShoppingModel;
import app.requests.params.HandlerErrorProductParams;
import app.requests.params.UpdateProductParams;
import app.requests.response.ProductResponse;
import app.utils.Constants;
import app.utils.Logger;
import app.utils.Utils;

public class BackgroundServices {
    public static final String SLEEP_JOB = "全ての商品の出品処理が終わりました。";
    private static final String TAG = "BackgroundServices";
    private static BackgroundServices instance;
    private ExecutorService executorService = Executors.newCachedThreadPool();
    private IUploadTaskCallback callback;
    private YahooShoppingModel shoppingModel;
    private int timeSleep = 10 * 60;
    private boolean runService = false;
    private int totalProduct;
    private int totalProductUpload;
    private int totalProductUpdate;
    private int totalProductFailure;

    private String host, accountId;

    private WebDriver currentWebDriver;

    public static BackgroundServices getInstance() {
        if (instance == null) {
            instance = new BackgroundServices();
        }
        return instance;
    }

    public void initData(String host, String accountId) {
        this.host = host;
        this.accountId = accountId;
        Logger.d(TAG, "#SERVER :" + host + ", version : " + Constants.APP_VERSION_CODE);
    }

    private LinkedList<ProductUpload> listProductUpload = new LinkedList<>();


    public void addProduct(ProductUpload productUpload) {
        this.listProductUpload.add(productUpload);
    }

    public void addProducts(List<ProductUpload> list) {
        this.listProductUpload.addAll(list);
    }


    private int maxItem = 0;

    public void startServiceUpload() {
        Logger.d(TAG, "#startServiceUpload :" + runService);
        if (!runService) {
            return;
        }
        executorService.submit(() -> {
            if (listProductUpload.size() > 0) {
                Logger.d(TAG, "======================================START SERVICE===========================================");
                Logger.d(TAG, "amount = " + listProductUpload.size());
                ProductUpload productUpload = listProductUpload.poll();
                if (productUpload == null) {
                    startServiceUpload();
                    return;
                } else {

                    switch (productUpload.getPublishStatus()) {
                        case ProductUpload.STATUS_UPLOAD:
                            totalProductUpload++;
                            break;
                        case ProductUpload.STATUS_EDIT:
                            totalProductUpdate++;
                            break;
                    }

                    if (productUpload.getProductCategory() != null) {
                        String[] arrCat = productUpload.getProductCategory().split(",");
                        List<String> arrayCategory = new ArrayList<>();
                        arrayCategory.addAll(Arrays.asList(arrCat));
                        if (arrayCategory.size() < 3) {
                            Logger.d(TAG, "#CATEGORY FAILURE " + productUpload.toString());
                            productUpload.setMessageFailure(UploadTask.CATEGORY_PATH_FAILURE);
                            productUpload.setPublishStatus(ProductUpload.UPLOAD_FAILURE);
                            Utils.runOnUIThread(() -> {
                                callback.statusWorking(UploadTask.CATEGORY_PATH_FAILURE);
                            });
                            productUpload.setProductCategory("");
                            updateProduct(productUpload);
                            handleErrorProduct(productUpload);
                            startServiceUpload();
                            return;
                        }
                    }
                }

                Utils.runOnUIThread(() -> {
                    int currentIndex = maxItem - listProductUpload.size();
                    callback.statusWorking("======================================" + currentIndex + "/" + maxItem + "========================================");
                });
                if (productUpload.getPrice() == 0 && productUpload.getPublishStatus() == ProductUpload.STATUS_UPLOAD) {
                    String priceError = "販売価格は1円～99999999円を入力してください。";
                    Utils.runOnUIThread(() -> {
                        callback.statusWorking(UploadTask.START_UPLOAD);
                        callback.statusWorking(productUpload.getName());
                        callback.statusWorking(priceError);
                    });
                    productUpload.setMessageFailure(priceError);
                    productUpload.setPublishStatus(ProductUpload.UPLOAD_FAILURE);
                    updateProduct(productUpload);
                    return;
                }
                UploadTask uploadTask = new UploadTask(currentWebDriver, productUpload, new IUploadTaskCallback() {
                    @Override
                    public void statusWorking(String message) {
                        callback.statusWorking(message);
                    }

                    @Override
                    public void onDoneTask(ProductUpload object) {
                        Logger.d(TAG, "#onDoneTask");
                        if (object.getPublishStatus() != ProductUpload.STATUS_SYSTEM_ERROR) {
                            updateProduct(object);
                            if (productUpload.getProductCategory().isEmpty()) {
                                handleErrorProduct(productUpload);
                            }
                        } else {
                            Utils.runOnUIThread(() -> {
                                callback.statusWorking(productUpload.getMessageFailure());
                                callback.serviceRunning(false);
                            });
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        startServiceUpload();
                        callback.onFailure(message);
                    }

                    @Override
                    public void serviceRunning(boolean isStart) {

                    }
                });
                uploadTask.run();
            } else {
                getListProductUpload();
            }
        });
    }

    private Timer timer;
    private int second;

    private void sleepJob() {
        Logger.d(TAG, "======================================SLEEP NOW===========================================");
        callback.statusWorking(SLEEP_JOB);
        callback.serviceRunning(false);
        setRunService(false);
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                second++;
                if (second == timeSleep) {
                    second = 0;
                    Logger.d(TAG, "#Time out 600s");
                    Utils.runOnUIThread(() -> callback.serviceRunning(true));
                    timer.cancel();
                }
            }
        }, 1000, 1000);

    }


    public void stopCountDown() {
        if (timer != null) {
            Logger.d(TAG, "#Run Now");
            second = 0;
            timer.cancel();
        }
    }

    private void getListProductUpload() {
        shoppingModel.getListProductUpload(host, accountId, new IBaseCallback<ProductResponse>() {
            @Override
            public void onResponse(ProductResponse response) {
                List<ProductUpload> listProductUpload = new ArrayList<>();
                if (response != null && response.getData() != null) {
                    listProductUpload = response.getData();
                    Logger.d(TAG, "#getListProductUpload number upload " + listProductUpload.size());
                    addProducts(listProductUpload);
                } else {
                    Logger.d(TAG, "#getListProductUpload Failure ");
                }
                maxItem = listProductUpload.size();
                if (maxItem == 0) {
                    sleepJob();
                } else {
                    Logger.d(TAG, "#SERVER :" + host + ", version : " + Constants.APP_VERSION_CODE);
                    startServiceUpload();
                }
            }

            @Override
            public void onError(String message) {
                Logger.d(TAG, "getListProductUpload onError " + message);
                callback.onFailure(message);
                sleepJob();
            }
        });
    }

    private void updateProduct(ProductUpload productUpload) {
        UpdateProductParams updateProductParams = new UpdateProductParams(accountId, productUpload);
        if (!productUpload.getMessageFailure().isEmpty()) {
            totalProductFailure++;
        }
        totalProduct++;
        String format = "#COUNT totalProduct : %d , totalUpload : %d , totalUpdate : %d , totalFailure : %d";
        Logger.d(TAG, String.format(format, totalProduct, totalProductUpload, totalProductUpdate, totalProductFailure));
        shoppingModel.updateProduct(host, updateProductParams, new IBaseCallback<BaseResponse>() {
            @Override
            public void onResponse(BaseResponse response) {
                if (response.isSuccess()) {
                    Logger.d(TAG, "#updateProduct success");
                } else {
                    Logger.d(TAG, "#updateProduct failure" + response.getMessage());
                }
                callback.onDoneTask(productUpload);
                startServiceUpload();
            }

            @Override
            public void onError(String message) {
                Logger.d(TAG, "#updateProduct onError" + message);
                callback.onDoneTask(productUpload);
                startServiceUpload();
            }
        });
    }

    private void handleErrorProduct(ProductUpload productUpload) {
        HandlerErrorProductParams params = new HandlerErrorProductParams(accountId, productUpload, HandlerErrorProductParams.CATEGORY_ERROR_CODE);
        shoppingModel.handleErrorProduct(host, params, new IBaseCallback<BaseResponse>() {
            @Override
            public void onResponse(BaseResponse response) {
                Logger.d(TAG, "#handleErrorProduct onResponse : " + response.isSuccess());
            }

            @Override
            public void onError(String message) {
                Logger.d(TAG, "#handleErrorProduct onError : " + message);
            }
        });
    }


    public void onLogout() {
        Logger.d(TAG, "onLogout");
        totalProduct = 0;
        totalProductUpload = 0;
        totalProductUpdate = 0;
        totalProductFailure = 0;
        listProductUpload.clear();
    }

    public void setCallback(IUploadTaskCallback callback) {
        this.callback = callback;
    }

    public void setShoppingModel(YahooShoppingModel shoppingModel) {
        this.shoppingModel = shoppingModel;
    }

    public void setCurrentWebDriver(WebDriver currentWebDriver) {
        this.currentWebDriver = currentWebDriver;
    }

    public WebDriver getCurrentWebDriver() {
        return currentWebDriver;
    }

    public void setRunService(boolean runService) {
        if (!runService) {
            listProductUpload.clear();
        }
        this.runService = runService;
    }
}
