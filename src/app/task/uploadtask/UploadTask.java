package app.task.uploadtask;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import app.App;
import app.entity.ProductUpload;
import app.task.BaseTask;
import app.utils.DateConvert;
import app.utils.Utils;


public class UploadTask extends BaseTask<IUploadTaskCallback> {
    private final String TAG = "UploadTask";
    public static final String START_UPLOAD = "出品開始";
    private static final String START_UPDATE = "出品処理開始";

    private static final int DELAY_JOB = 60;
    private static final String MESSAGE_DELAY_JOG = "反映またはアップロード中のため更新ができません。";
    private static final String MESSAGE_STOP_JOB = "一時停止します。";
    public static final String CATEGORY_PATH_FAILURE = "Yahoo!ショッピングカテゴリのサブカテゴリを選択してください。";
    private static final String TYPE_IMAGE_FAILURE = "JPEG、GIF画像のみ利用可能です。";
    private static String baseDirecterImage = System.getProperty("user.dir") + "/images/";
    private ProductUpload productUpload;
    private int count = 1;
    private String messageError = "";


    private static final String FORMAT_1 = "#%s";
    private static final String FORMAT_2 = "\t \t %s";
    private static final String FORMAT_3 = "\t \t \t %s";

    @Override
    public void print(String type, String message) {
        super.print(TAG, String.format(type, message));
    }

    @Override
    public String getTag() {
        return TAG;
    }

    public UploadTask(WebDriver webDriver, ProductUpload productUpload, IUploadTaskCallback callback) {
        super(webDriver, callback);
        this.productUpload = productUpload;
    }

    @Override
    public void run() {
        LinkedList<String> listImage = new LinkedList<>();
        print(FORMAT_1, productUpload.toString());
        if (productUpload.getPublishStatus() == ProductUpload.STATUS_UPLOAD) {
            String url = App.getInstance().getPreference().getUrlLoginYahoo();
            openUrl(url);
            WebElement systemError = getElementByClassName("dcSystemerrorttl");
            if (systemError != null) {
                String textError = systemError.getText() + "システムエラーが発生したため、現在、ご利用いただけません。\n 申し訳ございませんが、しばらく経ってから再度お試しください";
                productUpload.setMessageFailure(textError);
                productUpload.setPublishStatus(ProductUpload.STATUS_SYSTEM_ERROR);
                getCallback().onDoneTask(productUpload);
                return;
            }
            WebElement webItemCode = getElementByName("_itemcode");
            if (webItemCode != null) {
                count++;
                if (count > 3) {
                    print(FORMAT_1, "TRY COUNT " + count + " next product ");
                    int status = ProductUpload.UPLOAD_FAILURE;
                    productUpload.setPublishStatus(status);
                    productUpload.setMessageFailure(messageError.isEmpty() ? "System error" : messageError);
                    getCallback().onDoneTask(productUpload);
                    count = 1;
                    messageError = "";
                    return;
                }


                String code = webItemCode.getAttribute("value");
                productUpload.setItemCode(code);
                print(FORMAT_1, "#ITEM_CODE = " + code);
                List<String> listImageYahoo = productUpload.getImages();
                if (listImageYahoo != null && listImageYahoo.size() > 0) {
                    if (listImageYahoo.size() > 6) {
                        listImage.addAll(listImageYahoo.subList(0, 6));
                    } else {
                        listImage.addAll(productUpload.getImages());
                    }
                    downloadImage(listImage, this::startUpload);
                } else {
                    startUpload(listImageYahoo);
                }
            } else {
                count++;
                if (count <= 3) {
                    wait(2, TimeUnit.SECONDS);
                    run();
                } else {
                    count = 1;
                    messageError = "";
                    print(FORMAT_2, "Item Code null");
                    print(FORMAT_2, "#currentURL = " + getWebDriver().getCurrentUrl());
                    isAlertPresent();
                    productUpload.setMessageFailure("Item Code null");
                    productUpload.setPublishStatus(ProductUpload.UPLOAD_FAILURE);
                    getCallback().onDoneTask(productUpload);
                }
            }
        } else {
            startUpdate();
        }
    }

    private void downloadImage(LinkedList<String> listImage, ICallbackDownloadImage callbackDownLoadImage) {
        try {
            FileUtils.cleanDirectory(new File(baseDirecterImage));
        } catch (IOException e) {
            e.printStackTrace();
        }
        print(FORMAT_1, "#DOWLOAD IMAGE");
        List<String> pathsImage = new ArrayList<>();
        while (listImage.size() > 0) {
            String url = listImage.poll();
            String urlImage = App.getInstance().getPreference().getHost() + url;
            long timeCurrent = System.currentTimeMillis();
            String pathImage = baseDirecterImage + timeCurrent + ".jpg";
            try (BufferedInputStream in = new BufferedInputStream(new URL(urlImage).openStream());
                 FileOutputStream fileOutputStream = new FileOutputStream(pathImage)) {
                byte dataBuffer[] = new byte[1024];
                int bytesRead;
                while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
                    fileOutputStream.write(dataBuffer, 0, bytesRead);
                }
                print(FORMAT_2, "download image " + (pathsImage.size() + 1));
                pathsImage.add(pathImage);
            } catch (IOException e) {
                print(FORMAT_2, "download image failure " + e.toString());
            }
        }
        callbackDownLoadImage.onFinishDownload(pathsImage);
    }

    private void startUpload(List<String> imagesLocal) {
        updateStatus(START_UPLOAD);
        updateStatus(productUpload.getName());
        print(FORMAT_1, "#FILL DATA UPLOAD");
        fillProductName();
        fillPrice();
        fillDate();
        fillDescription1();
        fillDescription2();
        fillQuantity();
        fillPaths();
        fillCategory();
        fillCondition();
        fillImages(imagesLocal);
        if (!productUpload.getMessageFailure().isEmpty()) {
            int status = productUpload.getMessageFailure().isEmpty() ? ProductUpload.STATUS_DONE : ProductUpload.UPLOAD_FAILURE;
            productUpload.setPublishStatus(status);
            getCallback().onDoneTask(productUpload);
            return;
        }
        fillPublic();
        submitForm();
    }

    private void startUpdate() {
        updateStatus(START_UPDATE);
        updateStatus(productUpload.getName());
        String url = App.getInstance().getPreference().getUrlLoginYahoo();
        String urlUpdate = url + productUpload.getItemCode();
        openUrl(urlUpdate);
        WebElement systemError = getElementByClassName("dcSystemerrorttl");
        if (systemError != null) {
            String textError = systemError.getText() + "システムエラーが発生したため、現在、ご利用いただけません。\n 申し訳ございませんが、しばらく経ってから再度お試しください";
            productUpload.setMessageFailure(textError);
            productUpload.setPublishStatus(ProductUpload.STATUS_SYSTEM_ERROR);
            getCallback().onDoneTask(productUpload);
            return;
        }


        print(FORMAT_1, "#FILL DATA UPDATE");
        wait(2, TimeUnit.SECONDS);
        WebElement inputProductName = getElementByClassName("elForm_L");
        if (inputProductName != null) {
            String productName = inputProductName.getAttribute("value");
            if (productName.isEmpty()) {
                print(FORMAT_2, "productUpload not found");
                productUpload.setPublishStatus(ProductUpload.STATUS_DEFAULT);
                productUpload.setMessageFailure("ProductUpload not found");
                getCallback().onDoneTask(productUpload);
            } else {
                if (productUpload.getPrice() > 0) {
                    fillPrice();
                }
                wait(1, TimeUnit.SECONDS);
                fillQuantity();
                submitForm();
            }
        } else {
            print(FORMAT_2, "inputProductName null");
        }
    }

    private void fillProductName() {
        print(FORMAT_2, "Fill title");
        WebElement inputProductName = getElementByClassName("elForm_L");
        if (inputProductName != null) {
            inputProductName.click();
            inputProductName.clear();
            inputProductName.sendKeys(productUpload.getName());
        } else {
            print(FORMAT_3, "inputProductName null");
        }
    }

    private void fillPrice() {
        print(FORMAT_2, "Fill price");
        WebElement inputPrice = getElementByName("price");
        if (inputPrice != null) {
            inputPrice.click();
            inputPrice.clear();
            inputPrice.sendKeys(String.valueOf(productUpload.getPrice()));
        } else {
            print(FORMAT_3, "inputPrice null");
        }
    }

    private void fillTax() {
        print(FORMAT_2, "Fill taxable");
        List<WebElement> listTax = getElementsByName("taxable");
        if (listTax != null) {
            print(FORMAT_3, "listTax size = " + listTax.size());
            for (WebElement element : listTax) {
                print(FORMAT_3, "text = " + element.getAttribute("value") + " / " + productUpload.isTax());
                if (element.getAttribute("value").equals("1") && productUpload.isTax()) {
                    //co thue
                    print(FORMAT_3, "has tax " + listTax.size());
                    element.click();
                    break;
                }
            }
        } else {
            print(TAG, "listTax null");
        }
    }

    private void fillDate() {
        if (productUpload.getStartTime() != null) {
            print(FORMAT_2, "Fill startTimeField");
            WebElement startTime = getElementById("startTimeField");
            if (startTime != null) {
                String start = DateConvert.formatDate(productUpload.getStartTime(), DateConvert.FORMAT_2, DateConvert.FORMAT_1);
                startTime.click();
                startTime.clear();
                print(FORMAT_3, "fill startTime " + start);
                startTime.sendKeys(start);
            } else {
                print(FORMAT_3, "startTime null");
            }
        }
        if (productUpload.getEndTime() != null) {
            print(FORMAT_2, "Fill endTimeField");
            WebElement endTime = getElementById("endTimeField");
            if (endTime != null) {
                String end = DateConvert.formatDate(productUpload.getEndTime(), DateConvert.FORMAT_2, DateConvert.FORMAT_1);
                endTime.click();
                endTime.clear();
                print(FORMAT_3, "fill Time " + end);
                endTime.sendKeys(end);
            } else {
                print(FORMAT_3, "endTime null");
            }
        }
    }

    private void fillDescription1() {
        print(FORMAT_2, "Fill explanation");
        WebElement textAreaExplanation = getElementByName("explanation");
        if (textAreaExplanation != null) {
            textAreaExplanation.click();
            textAreaExplanation.clear();
            textAreaExplanation.sendKeys(productUpload.getDescription1());
        } else {
            print(FORMAT_3, "explanation null");
        }
    }

    private void fillDescription2() {
        print(FORMAT_2, "Fill caption");
        WebElement textAreaCaption = getElementByName("caption");
        if (textAreaCaption != null) {
            textAreaCaption.click();
            textAreaCaption.clear();
            textAreaCaption.sendKeys(productUpload.getDescription2());
        } else {
            print(FORMAT_3, "caption null");
        }
    }

    private void fillQuantity() {
        print(FORMAT_2, "Fill quantity");
        WebElement divQuantity = getElementById("div_quantity");
        WebElement inputQuantity = getElementByClassName(divQuantity, "elForm_XS");
        if (inputQuantity != null) {
            inputQuantity.click();
            inputQuantity.clear();
            inputQuantity.sendKeys(String.valueOf(productUpload.getQuantity()));
        } else {
            print(FORMAT_3, "inputQuantity null");
        }
    }

    private void fillPaths() {
        print(FORMAT_2, "Fill path");
        String paths = productUpload.getPath();
        if (paths != null) {
            String[] arrPath = paths.split(":");
            if (arrPath.length > 0) {
                String firstPath = arrPath[0].trim();
                WebElement idListCatTd = getElementById("idListCatTd");
                if (idListCatTd != null) {
                    List<WebElement> listCheckBox = getElementsByName(idListCatTd, "page_key[]");
                    boolean isExitsCate = false;
                    if (listCheckBox != null && listCheckBox.size() > 0) {
                        for (WebElement checkBox : listCheckBox) {
                            String value = checkBox.getAttribute("value");
                            if (value.trim().equals(firstPath)) {
                                isExitsCate = true;
                                checkBox.click();
                                break;
                            }
                        }
                    }
                    if (!isExitsCate) {
                        //add new cat id
                        WebElement inputCat = getElementById(idListCatTd, "moreCategory");
                        if (inputCat != null) {
                            inputCat.sendKeys(firstPath);
                            WebElement buttonAddCat = getElementById(idListCatTd, "addMoreCate");
                            if (buttonAddCat != null) {
                                buttonAddCat.click();
                                wait(1, TimeUnit.SECONDS);
                                getWebDriver().switchTo().alert().accept();
                            } else {
                                print(FORMAT_3, "addMoreCate null");
                            }
                        } else {
                            print(FORMAT_3, "inputCat null");
                        }
                    }
                } else {
                    print(FORMAT_3, "idListCatTd null");
                }
            }
        }
    }

    private void fillCategory() {
        print(FORMAT_2, "Fill yahoo category");
        if (productUpload.getProductCategory() != null) {
            print(FORMAT_3, "list category before : " + productUpload.getProductCategory());
            String[] arrCat = productUpload.getProductCategory().split(",");
            List<String> arrayCategory = new ArrayList<>();
            arrayCategory.addAll(Arrays.asList(arrCat));
            if (arrayCategory.size() > 0) {
                arrayCategory.remove(0);
                arrayCategory.remove(arrayCategory.size() - 1);
                print(FORMAT_3, "list category after : " + arrayCategory.toString());
                LinkedList<String> listCategory = new LinkedList(arrayCategory);
                if (listCategory.size() > 0) {
                    int index = 1;
                    while (listCategory.size() > 0) {
                        WebElement selectCategory;
                        String cateRoot;
                        if (index == 1) {
                            String idElement = "yhCat1";
                            cateRoot = listCategory.poll();
                            selectCategory = getElementById(idElement);
                        } else {
                            String selection = String.format("div#yhCatOrder%s > select", index);
                            cateRoot = listCategory.poll();

                            selectCategory = getElementByCssSelector(selection);
                        }
                        if (selectCategory != null) {
                            Select select = new Select(selectCategory);
                            for (WebElement webElement : select.getOptions()) {
                                String cate = webElement.getAttribute("value");
                                if (cate.equals(cateRoot)) {
                                    select.selectByValue(cate);
                                    break;
                                }
                            }
                            wait(2, TimeUnit.SECONDS);
                            index++;
                        } else {
                            print(FORMAT_3, "yahoo category null");
                        }
                    }
                } else {
                    print(FORMAT_3, "list category size = 0");
                }
            }

        }
    }

    private void fillCondition() {
        print(FORMAT_2, "Fill condition");
        List<WebElement> listCondition = getElementsByName("condition");
        if (listCondition != null) {
            for (WebElement element : listCondition) {
                print(FORMAT_3, "text = " + element.getAttribute("value") + " / " + productUpload.isNew());
                if (element.getAttribute("value").equals("0") && productUpload.isNew()) {
                    //sp moi
                    element.click();
                    break;
                }
            }
        } else {
            print(FORMAT_3, "listCondition null");
        }
    }

    private void fillImages(List<String> imagesLocal) {
        print(FORMAT_2, "Fill image");
        if (imagesLocal != null) {
            print(FORMAT_3, "list image size = " + imagesLocal.size());
            for (String image : imagesLocal) {
                int index = imagesLocal.indexOf(image);
                WebElement webElement = getElementById(String.format("uploadfile%d", index));
                if (webElement != null) {
                    print(FORMAT_3, "upload image " + (index + 1));
                    webElement.sendKeys(image);
                    wait(2, TimeUnit.SECONDS);
                    WebElement elementLoading = getElementById("loadingImg");
                    if (elementLoading != null) {
                        int countSecond = 0;
                        while (!isAlertPresent() && elementLoading.isDisplayed() && countSecond < 30) {
                            print(FORMAT_3, "progress upload image display : " + elementLoading.isDisplayed());
                            wait(2, TimeUnit.SECONDS);
                            countSecond++;
                            print(FORMAT_3, "countSecond : " + countSecond);
                        }
                        if (countSecond >= 30) {
                            print(FORMAT_3, "#LOOP FOREVER, UPLOAD AGAIN");
                            messageError = productUpload.getMessageFailure();
                            productUpload.setMessageFailure("");
                            run();
                            return;
                        }
                        if (isAlertPresent()) {
                            String message = getWebDriver().switchTo().alert().getText();
                            print(FORMAT_3, "show Alert message: " + message);
                            updateStatus(message);
                            switch (message) {
                                case TYPE_IMAGE_FAILURE:
                                    productUpload.setMessageFailure(message);
                                    return;
                                default:
                                    break;
                            }
                            updateStatus(MESSAGE_STOP_JOB);
                            wait(2, TimeUnit.SECONDS);
                            getWebDriver().switchTo().alert().accept();
                            print(FORMAT_3, "#DELAY  " + DELAY_JOB);
                            wait(DELAY_JOB, TimeUnit.SECONDS);
                            fillImages(imagesLocal);
                            break;
                        }
                    } else {
                        print(FORMAT_3, "progressLoading null, maybe alert appear");
                        if (isAlertPresent()) {
                            String message = getWebDriver().switchTo().alert().getText();
                            print(FORMAT_3, "show Alert message: " + message);
                            updateStatus(message);
                            switch (message) {
                                case TYPE_IMAGE_FAILURE:
                                    productUpload.setMessageFailure(message);
                                    return;
                                default:
                                    break;
                            }
                            updateStatus(MESSAGE_STOP_JOB);
                            wait(2, TimeUnit.SECONDS);
                            getWebDriver().switchTo().alert().accept();
                            print(FORMAT_3, "DELAY  " + DELAY_JOB);
                            wait(DELAY_JOB, TimeUnit.SECONDS);
                            fillImages(imagesLocal);
                            break;
                        } else {
                            print(FORMAT_3, "Alert not appear");
                        }
                    }
                } else {
                    print(FORMAT_3, "WebElement image null at position = " + (index + 1) + " , maybe alert appear");
                    if (isAlertPresent()) {
                        String message = getWebDriver().switchTo().alert().getText();
                        print(FORMAT_3, "show Alert message: " + message);
                        updateStatus(message);
                        switch (message) {
                            case TYPE_IMAGE_FAILURE:
                                productUpload.setMessageFailure(message);
                                return;
                            default:
                                break;
                        }
                        updateStatus(MESSAGE_STOP_JOB);
                        wait(2, TimeUnit.SECONDS);
                        getWebDriver().switchTo().alert().accept();
                        print(FORMAT_3, "#DELAY  " + DELAY_JOB);
                        wait(DELAY_JOB, TimeUnit.SECONDS);
                        fillImages(imagesLocal);
                        break;
                    } else {
                        print(FORMAT_3, "Alert not appear");
                    }

                }
            }
        } else {
            print(FORMAT_3, "list image empty");
        }
    }


    private void fillPublic() {
        print(FORMAT_2, "Fill private productUpload");
        WebElement cbPublic = getElementByClassName("cb-enable");//cb_disable
        if (cbPublic != null) {
            cbPublic.click();
        } else {
            print(FORMAT_3, "cbPrivate null");
        }
    }

    private void submitForm() {
        print(FORMAT_2, "SUBMIT FORM");
        WebElement boxSubmit = getElementByClassName("mdBtnbox");
        if (boxSubmit != null) {
            WebElement submitForm = getElementByClassName(boxSubmit, "dcSubmit");
            if (submitForm != null) {
                submitForm.click();
                WebElement elementError = getElementById("mdErrormassage");
                if (elementError != null) {
                    print(FORMAT_3, "An error occurred");
                    List<WebElement> listError = getElementsByClassName("dcErrortxt");
                    if (listError != null) {
                        for (WebElement error : listError) {
                            String messageFailure = error.getText();
                            if (messageFailure.equals(CATEGORY_PATH_FAILURE)) {
                                productUpload.setProductCategory("");
                            }
                            updateStatus(messageFailure);
                            productUpload.setMessageFailure(messageFailure);
                        }
                    }
                } else {
                    print(FORMAT_3, "check waitingImage display");
                    WebElement elementWaiting = getElementById("waitingImage");
                    if (elementWaiting != null) {
                        int countSecond = 0;
                        while (!isAlertPresent() && elementWaiting.isDisplayed() && countSecond < 12) {
                            print(FORMAT_3, "check waitingImage display : " + elementWaiting.isDisplayed());
                            wait(5, TimeUnit.SECONDS);
                            countSecond++;
                            print(FORMAT_3, "countSecond : " + countSecond);
                        }
                        if (countSecond >= 12) {
                            print(FORMAT_3, "#LOOP FOREVER, UPLOAD AGAIN");
                            messageError = productUpload.getMessageFailure();
                            productUpload.setMessageFailure("");
                            run();
                            return;
                        }
                        wait(2, TimeUnit.SECONDS);
                        if (isAlertPresent()) {
                            String message = getWebDriver().switchTo().alert().getText();
                            updateStatus(message);
                            print(FORMAT_3, "show Alert message" + message);
                            if (message != null && message.equals(MESSAGE_DELAY_JOG)) {
                                updateStatus(MESSAGE_STOP_JOB);
                                getWebDriver().switchTo().alert().accept();
                                print(FORMAT_3, "DELAY  " + DELAY_JOB);
                                wait(DELAY_JOB / 3, TimeUnit.SECONDS);
                                submitForm();
                            } else {
                                getWebDriver().switchTo().alert().accept();
                                productUpload.setMessageFailure(message);
                                wait(2, TimeUnit.SECONDS);
                            }
                        }

                        WebElement webElement = getElementById("mdSavecomplete");
                        if (webElement == null) {
                            WebElement errorMessage = getElementByCssSelector("div#mdErrormassage > span");
                            String message = "入力内容をご確認ください";
                            if (errorMessage != null) {
                                message = errorMessage.getText();
                            }
                            productUpload.setMessageFailure(message);
                            updateStatus(message);
                        } else {
                            print(FORMAT_3, "upload SUCCESS");
                        }
                    } else {
                        print(FORMAT_3, "waitingImage null");
                    }
                }
            } else {
                print(FORMAT_3, "submitForm null");
            }
        } else {
            print(FORMAT_3, "boxSubmit null");
        }
        if (!productUpload.getMessageFailure().isEmpty()) {
            print(FORMAT_3, "#Message failure : " + productUpload.getMessageFailure());
        }
        count = 1;
        messageError = "";
        int status = productUpload.getMessageFailure().isEmpty() ? ProductUpload.STATUS_DONE : ProductUpload.UPLOAD_FAILURE;
        productUpload.setPublishStatus(status);
        getCallback().onDoneTask(productUpload);
    }

    public interface ICallbackDownloadImage {
        void onFinishDownload(List<String> pathsImage);
    }

    private void updateStatus(String status) {
        Utils.runOnUIThread(() -> {
            getCallback().statusWorking(status);
        });

    }
}
