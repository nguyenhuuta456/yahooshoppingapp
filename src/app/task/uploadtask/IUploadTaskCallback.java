package app.task.uploadtask;

import app.task.IBaseTaskCallback;
import app.entity.ProductUpload;

public interface IUploadTaskCallback extends IBaseTaskCallback<ProductUpload> {

    void statusWorking(String message);

    void serviceRunning(boolean isStart);

}
