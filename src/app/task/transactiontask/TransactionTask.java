package app.task.transactiontask;

import app.App;
import app.entity.Page;
import app.entity.Transaction;
import app.task.BaseTask;
import app.utils.Constants;
import app.utils.Utils;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class TransactionTask extends BaseTask<ITransactionTask> {
    private static final String TAG = "TransactionTask";
    private static String MESSAGE_NO_RESULT = "表示出来る項目がありません。";
    private boolean hasSetCookie = false;

    public TransactionTask(WebDriver webDriver, ITransactionTask callback) {
        super(webDriver, callback);
    }

    private String currentUrl = Constants.EMPTY;

    public String getTag() {
        return TAG;
    }

    @Override
    public void run() {
        super.run();
        Utils.runOnUIThread(() -> getCallback().showLoading(true));
        if (!hasSetCookie) {
            openUrl(currentUrl);
            Set<Cookie> cookies = App.getInstance().getCookieFromFile();
            for (Cookie cookie : cookies) {
                if (cookie != null) {
                    getWebDriver().manage().addCookie(cookie);
                }
            }
            hasSetCookie = true;
        }
        openUrl(currentUrl);
        List<Transaction> listTransaction = getListProduct();
        List<Page> listPage = getPages();
        Utils.runOnUIThread(() -> {
            if (listTransaction.size() > 0) {
                getCallback().onDoneTask(listTransaction);
            } else {
                getCallback().emptyTransaction(MESSAGE_NO_RESULT);
            }
            getCallback().listPage(listPage);
        });
        Utils.runOnUIThread(() -> getCallback().showLoading(false));
    }

    private List<Transaction> getListProduct() {
        List<Transaction> listTransaction = new ArrayList<>();
        WebElement tableTransaction = getElementByCssSelector("form#history_list > div.elPagehead > table");
        if (tableTransaction != null) {
            List<WebElement> listTagTr = getElementsByCssSelector(tableTransaction, "tr");
            if (listTagTr != null && listTagTr.size() > 0) {
                listTagTr.remove(0);
                listTagTr.forEach(tagTr -> {
                    Transaction transaction = new Transaction();
                    WebElement tagDate = getElementByCssSelector(tagTr, "td.elDate");
                    if (tagDate != null) {
                        transaction.setTime(tagDate.getText());
                    } else {
                        print(TAG, "tagDate null");
                    }

                    WebElement tagOrderNumber = getElementByCssSelector(tagTr, "td.elOrder-number");
                    if (tagOrderNumber != null) {
                        WebElement tagDetail = getElementByCssSelector(tagOrderNumber, "a");
                        if (tagDetail != null) {
                            String urlDetail = tagDetail.getAttribute("href");
                            transaction.setUrlDetailProduct(urlDetail);
                        }

                        transaction.setOrderNumber(tagOrderNumber.getText());
                    } else {
                        print(TAG, "tagOrderNumber null");
                    }

                    WebElement tagProductName = getElementByCssSelector(tagTr, "td.elItem");
                    if (tagProductName != null) {
                        transaction.setProductName(tagProductName.getText());
                    } else {
                        print(TAG, "tagProductName null");
                    }

                    WebElement tagUserName = getElementByCssSelector(tagTr, "td.elUser");
                    if (tagUserName != null) {
                        transaction.setUserName(tagUserName.getText());
                    } else {
                        print(TAG, "tagUserName null");
                    }

                    WebElement tagPrice = getElementByCssSelector(tagTr, "td.elPrice");
                    if (tagPrice != null) {
                        transaction.setPrice(tagPrice.getText());
                    } else {
                        print(TAG, "tagPrice null");
                    }

                    WebElement tagImageStatus = getElementByCssSelector(tagTr, "td.elStatus");
                    if (tagImageStatus != null) {
                        WebElement tagImage = getElementByCssSelector(tagImageStatus, "span.icon > img");
                        if (tagImage != null) {
                            String urlImage = tagImage.getAttribute("src");
                            String status = tagImage.getAttribute("alt");
                            transaction.setImageStatus(urlImage);
                            transaction.setStatusProduct(status);
                        } else {
                            print(TAG, "tagImage null");
                        }
                    } else {
                        print(TAG, "tagImageStatus null");
                    }

                    listTransaction.add(transaction);
                });

            } else {
                print(TAG, "listTagTr null");
            }
        } else {
            print(TAG, "tableTransaction null");
        }
        return listTransaction;

    }

    private List<Page> getPages() {
        WebElement tagPages = getElementByClassName("mdPagenation");
        List<Page> listPages = new ArrayList<>();
        if (tagPages != null && tagPages.isDisplayed()) {
            List<WebElement> listPage = getElementsByCssSelector(tagPages, "li");
            if (listPage != null && listPage.size() > 0) {
                listPage.forEach(tagPage -> {
                    String className = tagPage.getAttribute("class");
                    if (className.equals("active") || className.equals("elSkip")) {
                        Page objectPage = new Page(tagPage.getText(), null);
                        listPages.add(objectPage);
                        objectPage.setSkip(className.equals("elSkip"));
                    } else {
                        WebElement page = getElementByCssSelector(tagPage, "a");
                        if (page != null) {
                            String numberPage = page.getText();
                            String urlPage = page.getAttribute("href");
                            Page objectPage = new Page(numberPage, urlPage);
                            listPages.add(objectPage);
                        } else {
                            print(TAG, "#page null");
                        }
                    }
                });
            } else {
                print(TAG, "#listPage empty");
            }
        } else {
            print(TAG, "#tagPages null");
        }
        return listPages;
    }


    public void setCurrentUrl(String currentUrl) {
        this.currentUrl = currentUrl;
    }

    public String getCurrentUrl() {
        return getWebDriver().getCurrentUrl();
    }

    public void logoutApp() {
        WebDriver webDriver = getWebDriver();
        if (webDriver != null) {
            try {
                webDriver.manage().deleteAllCookies();
                webDriver.quit();
            } catch (WebDriverException e) {
                e.printStackTrace();
            }
        }
    }
}
