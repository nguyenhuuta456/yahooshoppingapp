package app.task.transactiontask;

import app.entity.Page;
import app.entity.Transaction;
import app.task.IBaseTaskCallback;

import java.util.List;

public interface ITransactionTask extends IBaseTaskCallback<List<Transaction>> {
    void showLoading(boolean isShowLoading);

    void listPage(List<Page> list);

    void emptyTransaction(String empty);

}
