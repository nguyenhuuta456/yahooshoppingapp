package app.task;

public interface IBaseTaskCallback<T> {

    void onDoneTask(T object);

    void onFailure(String message);
}
