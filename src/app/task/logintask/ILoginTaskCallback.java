package app.task.logintask;

import app.controller.loginapp.dialog.DialogCaptchaController;
import app.task.IBaseTaskCallback;
import app.entity.Preference;

public interface ILoginTaskCallback extends IBaseTaskCallback<Preference> {
    void hindLoading();

    void showDialogCaptcha(String message, String urlImage, DialogCaptchaController.ICaptchaCallback captchaCallback);
}
