package app.task.logintask;

import app.App;
import app.controller.loginapp.dialog.DialogCaptchaController;
import app.controller.loginapp.dialog.DialogCaptchaStage;
import app.entity.Preference;
import app.task.BaseTask;
import app.utils.Constants;
import app.utils.Logger;
import app.utils.Utils;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import sun.rmi.runtime.Log;

import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginYahooTask extends BaseTask<ILoginTaskCallback> {
    private final String TAG = "LoginYahooTask";
    public static final String URL_LOGIN_YAHOO = "https://creator.shopping.yahoo.co.jp/";
    private static final String URL_UPLOAD = "https://creator.shopping.yahoo.co.jp/%s/item/edit/";
    private Preference preference;
    private DialogCaptchaStage dialogCaptcha;

    public LoginYahooTask(WebDriver webDriver, ILoginTaskCallback callback) {
        super(webDriver, callback);
    }

    public void setAccount(Preference preference) {
        this.preference = preference;
    }

    @Override
    public void run() {
        Logger.d(TAG, "start login 2");
        String url = preference.getUrlLoginYahoo() == null ? URL_LOGIN_YAHOO : preference.getUrlLoginYahoo();
        openUrl(url);
        WebElement captchaBox = getElementById("captchaForm");
        if (captchaBox == null) {
            fillUserName();
            if (hasError()) {
                return;
            }
            fillPassword();
            checkLoginSuccess();
        } else {
            handleCaptcha1();
        }
    }

    public void run2() {
        Logger.d(TAG, "start login 2");
        WebElement captchaBox = getElementById("captchaForm");
        if (captchaBox == null) {
            fillUserName();
            if (hasError()) {
                return;
            }
            fillPassword();
            checkLoginSuccess();
        } else {
            handleCaptcha1();
        }
    }

    private void fillUserName() {
        wait(1, TimeUnit.SECONDS);
        WebElement inputUserName = getElementById("username");
        simulateSendKeys(inputUserName, preference.getAccountYahoo());
        new Actions(getWebDriver()).sendKeys(Keys.ENTER).build().perform();
        wait(1, TimeUnit.SECONDS);
    }

    private void fillPassword() {
        WebElement inputPassword = getElementById("passwd");
        simulateSendKeys(inputPassword, preference.getPasswordYahoo());
        wait(1, TimeUnit.SECONDS);
    }

    private boolean hasError() {
        WebElement elementError = getElementById("errMsg");
        if (elementError != null && elementError.isDisplayed()) {
            String message = elementError.getText();
            print(TAG, "#hasError : " + message);
            Utils.runOnUIThread(() -> getCallback().onFailure(message));
            return true;
        }
        return false;
    }

    private void checkLoginSuccess() {
        new Actions(getWebDriver()).sendKeys(Keys.ENTER).build().perform();
        wait(1, TimeUnit.SECONDS);
        if (hasError()) {
            return;
        }
        if (hasCaptcha()) {
            return;
        }
        String currentUrl = getWebDriver().getCurrentUrl();
        Logger.d(TAG, "#currentUrl " + currentUrl);
        if (currentUrl.matches("https://creator.shopping.yahoo.co.jp/.+/.+")) {

            String regex = "https://creator.shopping.yahoo.co.jp/(?<shopId>.+)/.+";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(currentUrl);
            String shopId = Constants.EMPTY;
            while (matcher.find()) {
                shopId = matcher.group("shopId");
            }
            preference.setShopId(shopId);
            print(TAG, "#ShopId = " + shopId);
            if (preference.getUrlLoginYahoo() == null) {
                goToScreenUpload(shopId);
            } else {
                Utils.runOnUIThread(() -> {
                    Logger.d(TAG, "#Login Success ");
                    getCallback().onDoneTask(preference);
                });
            }
        } else {
            handleCaptcha1();
        }
    }

    private void goToScreenUpload(String shopId) {
        if (!shopId.isEmpty()) {
            String urlUpload = String.format(URL_UPLOAD, shopId);
            openUrl(urlUpload);
            preference.setUrlLoginYahoo(urlUpload);
            Utils.runOnUIThread(() -> {
                Logger.d(TAG, "#Login Success ");
                getCallback().onDoneTask(preference);
            });
        } else {
            WebElement step1 = getElementById("numero4");
            if (step1 != null) {
                step1.click();
                waitDefault();
                WebElement step2 = getElementById("numero5");
                if (step2 != null) {
                    step2.click();
                    WebElement popupConfirm = getElementById("confirm");
                    if (popupConfirm != null && popupConfirm.isDisplayed()) {
                        wait(1, TimeUnit.SECONDS);
                        WebElement checkBox = getElementById(popupConfirm, "disableDialogBox");
                        if (checkBox != null) {
                            checkBox.click();
                        }
                        waitDefault();
                        WebElement closeBox = getElementByClassName(popupConfirm, "closeDialogBox");
                        if (closeBox != null) {
                            closeBox.click();
                        }
                        wait(1, TimeUnit.SECONDS);
                    }
                    waitDefault();
                    String currentUrl = getWebDriver().getCurrentUrl();
                    preference.setUrlLoginYahoo(currentUrl);
                    Logger.d(TAG, "#goToScreenUpload current url = " + currentUrl);
                    Utils.runOnUIThread(() -> {
                        Logger.d(TAG, "#Login Success ");
                        getCallback().onDoneTask(preference);
                    });
                } else {
                    Logger.d(TAG, "#goToScreenUpload step 2 null");
                }
            } else {
                Logger.d(TAG, "#goToScreenUpload step 1 null");
            }
        }
    }


    private void handleCaptcha1() {
        Logger.d(TAG, "#handleCaptcha1");
        WebElement captchaForm = getElementById("captchaForm");
        if (captchaForm != null && captchaForm.isDisplayed()) {
            WebElement tagImage = getElementById(captchaForm, "captchaV5MultiByteCaptchaImg");
            if (tagImage != null) {
                String imageCaptcha = tagImage.getAttribute("src");
                String finalMessage = getElementByClassName(captchaForm, "agreement_text").getText();
                Utils.runOnUIThread(() -> {
                    getCallback().hindLoading();
                    getCallback().showDialogCaptcha(finalMessage, imageCaptcha, captcha -> new Thread(() -> {
                        WebElement webElement = getElementById("captchaV5Answer");
                        simulateSendKeys(webElement, captcha);
                        WebElement buttonConfirm = getElementByCssSelector(captchaForm, "div.btnArea > input");
                        if (buttonConfirm != null) {
                            buttonConfirm.click();
                            run2();
                        } else {
                            Logger.d(TAG, "#buttonConfirm null");
                        }
                    }).start());
                });
            }
        } else {
            Logger.d(TAG, "#handleCaptcha1 captchaForm null");
        }
        Utils.runOnUIThread(() -> getCallback().hindLoading());
    }

    private boolean hasCaptcha() {
        WebElement captchaBox = getElementById("captchaBox");
        if (captchaBox != null && captchaBox.isDisplayed()) {
            WebElement contentError = getElementByCssSelector(captchaBox, "div.captchaArea");
            String message = "";
            if (contentError != null) {
                message = contentError.getText();
            }
            print(TAG, "message : " + message);
            WebElement element = getElementByCssSelector(captchaBox, "div.captchaImg > img");
            String image = null;
            if (element != null) {
                image = element.getAttribute("src");
                print(TAG, "image : " + image);
            } else {
                Logger.d(TAG, "#captchaImg  null");
            }
            String finalMessage = message;
            String finalImage = image;
            if (finalImage != null && !finalImage.isEmpty()) {
                Utils.runOnUIThread(() -> {
                    getCallback().hindLoading();
                    getCallback().showDialogCaptcha(finalMessage, finalImage, captcha -> new Thread(() -> {
                        fillPassword();
                        WebElement webElement = getElementById("secword");
                        simulateSendKeys(webElement, captcha);
                        checkLoginSuccess();
                    }).start());
                });
                print(TAG, "#hasCaptcha 2");
                return true;
            }
        } else {
            Logger.d(TAG, "#handleCaptcha1 captchaBox null");
        }
        return false;
    }

    private void showDialogCaptcha(String message, String urlImage, DialogCaptchaController.ICaptchaCallback captchaCallback) {
        if (dialogCaptcha == null) {
            dialogCaptcha = new DialogCaptchaStage(App.getInstance().getStageApp());
        }
        dialogCaptcha.getController().setCallback(captchaCallback);
        if (dialogCaptcha.isShow()) {
            dialogCaptcha.updateCaptcha(message, urlImage);
        } else {
            dialogCaptcha.show(message, urlImage);
        }
    }
}
