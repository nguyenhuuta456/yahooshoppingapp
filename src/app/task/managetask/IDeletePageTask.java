package app.task.managetask;

import app.entity.ProductManage;
import app.task.IBaseTaskCallback;

import java.util.List;

public interface IDeletePageTask extends IBaseTaskCallback<List<ProductManage>> {

    void showLoading(boolean isShow);

    void deleteItem(String itemCode, String crumb);

}
