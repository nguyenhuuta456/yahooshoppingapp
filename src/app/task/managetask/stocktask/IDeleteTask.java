package app.task.managetask.stocktask;

import app.task.IBaseTaskCallback;

public interface IDeleteTask extends IBaseTaskCallback<Object> {
    void deleteItem(String itemCode);
}
