package app.task.managetask.stocktask;

import app.App;
import app.entity.Preference;
import app.task.BaseTask;
import app.utils.Utils;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class DeleteTask extends BaseTask<IDeleteTask> {
    private static final String OUT_OF_STOCK = "在庫なし";
    private boolean hasSendCookie = false;

    public DeleteTask(WebDriver webDriver, IDeleteTask callback) {
        super(webDriver, callback);
    }

    @Override
    public String getTag() {
        return DeleteTask.class.getSimpleName();
    }

    @Override
    public void run() {
        super.run();
        Preference preference = App.getInstance().getPreference();
        String url = String.format("https://creator.shopping.yahoo.co.jp/%s/item/list/", preference.getShopId());
        if (!hasSendCookie) {
            openUrl(url);
            Set<Cookie> cookies = App.getInstance().getCookieFromFile();
            for (Cookie cookie : cookies) {
                if (cookie != null) {
                    getWebDriver().manage().addCookie(cookie);
                }
            }
            hasSendCookie = true;
        }
        openUrl(url);
        waitDefault();
        deleteProductOutOfStock();
    }

    private void deleteProductOutOfStock() {
        List<WebElement> tagAllItems = getElementsByCssSelector("#allItem > div");
        if (tagAllItems != null && tagAllItems.size() > 0) {
            tagAllItems.forEach(item -> {
                String idItem = item.getAttribute("id");
                WebElement tagStock = getElementByCssSelector(item, "li.elStock");
                if (tagStock != null) {
                    String stock = tagStock.getText();
                    if (stock.equals(OUT_OF_STOCK)) {
                        WebElement tagEdit = getElementByCssSelector(item, "li.elEdit");

                        if (tagEdit != null) {
                            List<WebElement> listEdit = getElementsByCssSelector(tagEdit, "a");
                            if (listEdit != null && listEdit.size() == 2) {
                                WebElement buttonDelete = listEdit.get(1);
                                String scriptDelete = buttonDelete.getAttribute("onclick");
                                print(TAG, "#Delete " + scriptDelete);
                                buttonDelete.click();
                                wait(1, TimeUnit.SECONDS);
                                print(TAG, "#Delete click");
                                while (isAlertPresent()) {
                                    wait(1, TimeUnit.SECONDS);
                                    getWebDriver().switchTo().alert().accept();
                                    print(TAG, "#Accept alert");
                                }
                                print(TAG, "#popup appear 1 " + isAlertPresent());
                                WebElement itemDelete = getElementById(idItem);
                                while (itemDelete != null) {
                                    wait(2, TimeUnit.SECONDS);
                                    if (isAlertPresent()) {
                                        print(TAG, "Alert in while :" + isAlertPresent());
                                        getWebDriver().switchTo().alert().accept();
                                        wait(1, TimeUnit.SECONDS);
                                        buttonDelete.click();
                                        wait(1, TimeUnit.SECONDS);
                                        getWebDriver().switchTo().alert().accept();
                                    }
                                    itemDelete = getElementById(idItem);
                                    print(TAG, idItem + " has delete : " + (itemDelete == null));
                                }
                                while (isAlertPresent()) {
                                    wait(1, TimeUnit.SECONDS);
                                    getWebDriver().switchTo().alert().accept();
                                    print(TAG, "#Alert appear outside logic");
                                }
                                Utils.runOnUIThread(() -> getCallback().deleteItem(idItem));
                            } else {
                                print(TAG, "listEdit null");
                            }
                        } else {
                            print(TAG, "tagEdit null");
                        }
                    }
                } else {
                    print(TAG, "tagStock null");
                }
            });
            wait(1, TimeUnit.SECONDS);
            nextPage();
        } else {
            print(TAG, "tagAllItems null");
            getCallback().onDoneTask(null);
        }
    }

    private void nextPage() {
        WebElement tagPages = getElementByClassName("mdPagenation");
        if (tagPages != null && tagPages.isDisplayed()) {
            List<WebElement> listPage = getElementsByCssSelector(tagPages, "li");
            if (listPage != null && listPage.size() > 0) {
                for (WebElement tagPage : listPage) {
                    String className = tagPage.getAttribute("class");
                    if (className.equals("active")) {
                        int position = listPage.indexOf(tagPage);
                        int size = listPage.size() - 1;
                        if (position == size) {
                            getCallback().onDoneTask(null);
                        } else {
                            tagPage = listPage.get(position + 1);
                            WebElement page = getElementByCssSelector(tagPage, "a");
                            if (page != null) {
                                try {
                                    int numberPage = Integer.parseInt(page.getText());
                                    print(TAG, "#current page " + numberPage);
                                    page.click();
                                    waitDefault();
                                    deleteProductOutOfStock();
                                    break;
                                } catch (NumberFormatException ex) {
                                    print(TAG, "#NumberFormatException " + ex.toString());
                                }

                            } else {
                                print(TAG, "#page null");
                            }
                        }
                    }
                }
            } else {
                getCallback().onDoneTask(null);
            }
        } else {
            getCallback().onDoneTask(null);
        }
    }
}
