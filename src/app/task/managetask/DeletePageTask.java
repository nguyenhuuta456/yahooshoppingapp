package app.task.managetask;

import app.App;
import app.task.BaseTask;
import app.utils.Constants;
import app.utils.Utils;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import javax.rmi.CORBA.Util;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DeletePageTask extends BaseTask<IDeletePageTask> {
    private static final String TAG = "DeletePageTask";

    public DeletePageTask(WebDriver webDriver, IDeletePageTask callback) {
        super(webDriver, callback);
    }

    private String currentUrl = Constants.EMPTY;

    public String getTag() {
        return TAG;
    }

    private boolean isFirst = true;

    private List<String> listJavaScrip;

    @Override
    public void run() {
        if (isFirst) {
            openUrl(currentUrl);
            Set<Cookie> cookies = App.getInstance().getCookieFromFile();
            for (Cookie cookie : cookies) {
                if (cookie != null) {
                    getWebDriver().manage().addCookie(cookie);
                }
            }
            isFirst = false;
        }
        openUrl(currentUrl);
        wait(1, TimeUnit.SECONDS);
        if (listJavaScrip != null) {
            deleteItem(0);
        } else {
            Utils.runOnUIThread(() -> getCallback().showLoading(false));
        }
    }

    private void deleteItem(int position) {
        if (position < listJavaScrip.size()) {
            String jsDelete = listJavaScrip.get(position);
            executeJavaScript(jsDelete);
            while (isAlertPresent()) {
                wait(1, TimeUnit.SECONDS);
                getWebDriver().switchTo().alert().accept();
                print(TAG, "#Accept alert");
            }
            String regex = "\\('(?<itemCode>.*?)',";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(jsDelete);
            String itemCode = Constants.EMPTY;
            while (matcher.find()) {
                itemCode = matcher.group("itemCode");
            }
            String idItem = "it" + itemCode;
            WebElement itemDelete = getElementById(idItem);
            while (itemDelete != null) {
                wait(2, TimeUnit.SECONDS);
                if (isAlertPresent()) {
                    print(TAG, "Alert in while :" + isAlertPresent());
                    wait(1, TimeUnit.SECONDS);
                    getWebDriver().switchTo().alert().accept();
                    deleteItem(position);
                    return;
                }
                itemDelete = getElementById(idItem);
                print(TAG, idItem + " has delete : " + (itemDelete == null));
            }
            while (isAlertPresent()) {
                wait(1, TimeUnit.SECONDS);
                getWebDriver().switchTo().alert().accept();
                print(TAG, "#Alert appear outside logic");
            }
            Utils.runOnUIThread(() -> getCallback().deleteItem(idItem, null));
            deleteItem(position + 1);
        } else {
            Utils.runOnUIThread(() -> getCallback().deleteItem(null, null));
        }
    }


    public void setCurrentUrl(String currentUrl) {
        this.currentUrl = currentUrl;
    }

    public void setListJavaScrip(List<String> listJavaScrip) {
        this.listJavaScrip = listJavaScrip;
    }
}
