package app.database;

/**
 * Created by HieuPT on 11/13/2017.
 */
class DataContract {

    static class Preference {

        public static final String TABLE_NAME = "Preference";

        public static final String COLUMN_USER_ID = "id";

        public static final String COLUMN_IS_LOGIN = "IsLogin";

        public static final String COLUMN_HOST = "host";
        private Preference() {
        }
    }



    private DataContract() {
        //no instance
    }
}
