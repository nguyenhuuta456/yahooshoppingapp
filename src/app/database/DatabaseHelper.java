package app.database;

import com.sun.org.apache.xerces.internal.impl.io.UTF8Reader;
import app.utils.Constants;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.*;

public class DatabaseHelper {

    private static final String TAG = DatabaseHelper.class.getSimpleName();

    public static final String DIRECTORY = "data";
    public static final String DIRECTORY_IMAGE = "images";

    private static final String DB_NAME = "data.db";

    public static final String PREFERENCE = "preference.txt";

    public static final String DATABASE_VERSION = "database_version.txt";

    public static final String COOKIE = "cookie.txt";

    private static DatabaseHelper instance;

    public static DatabaseHelper getInstance() {
        if (instance == null) {
            instance = new DatabaseHelper();
        }
        return instance;
    }

    private Connection connection;


    private DatabaseHelper() {
        try {
            Class.forName("org.sqlite.JDBC");
            ensureDbDirectoryExist();
            connection = DriverManager.getConnection("jdbc:sqlite:" + DIRECTORY + "/" + DB_NAME);
        } catch (ClassNotFoundException | SQLException ignored) {
        }
    }

    private void ensureDbDirectoryExist() {
        File dbDirectory = new File(DIRECTORY);
        File images = new File(DIRECTORY_IMAGE);
        if (!dbDirectory.exists()) {
            dbDirectory.mkdir();
        }
        if (!images.exists()){
            images.mkdir();
        }
        File file = new File(DIRECTORY + "/" + PREFERENCE);
        File fileDatabase = new File(DIRECTORY + "/" + DATABASE_VERSION);
        File fileCookie = new File(DIRECTORY + "/" + COOKIE);

        try {
            file.createNewFile();
            fileDatabase.createNewFile();
            fileCookie.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void createTable() {
        try {
            Statement statement = connection.createStatement();
//            createTablePreference(statement);
        } catch (SQLException e) {
        }
    }

    public void onUpgrade() {
        int oldVersion = getVersionDatabase();
        int newVersion = Constants.DATABASE_VERSION;
        if (oldVersion < newVersion) {
            try {
                Statement statement = connection.createStatement();
//                if (newVersion == 2){
//                    String sql = "ALTER TABLE "+ DataContract.Preference.TABLE_NAME +" ADD COLUMN "+ DataContract.Preference.COLUMN_TEST +" INTEGER DEFAULT 12";
//                    statement.execute(sql);
//                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            saveVersionDatabase(String.valueOf(Constants.DATABASE_VERSION));
        }

    }

    private int getVersionDatabase() {
        try {
            BufferedReader bw = new BufferedReader(new UTF8Reader(new FileInputStream(DatabaseHelper.DIRECTORY + "/" + DatabaseHelper.DATABASE_VERSION)));
            String version = bw.readLine();
            if (version != null && !version.isEmpty()){
                return Integer.parseInt(version);
            }else{
                saveVersionDatabase(String.valueOf(Constants.DATABASE_VERSION));
                return Constants.DATABASE_VERSION;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private void saveVersionDatabase(String json) {
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(DatabaseHelper.DIRECTORY + "/" + DatabaseHelper.DATABASE_VERSION, false), StandardCharsets.UTF_8))) {
            bw.write(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//
//    private void createTablePreference(Statement statement) {
//        try {
//            String sql = "CREATE TABLE IF NOT EXISTS " + DataContract.Preference.TABLE_NAME
//                    + "("
//                    + DataContract.Preference.COLUMN_USER_ID + " INTEGER,"
//                    + DataContract.Preference.COLUMN_IS_LOGIN + " INTEGER,"
//                    + DataContract.Preference.COLUMN_HOST + " VARCHAR,"
//                    + DataContract.Preference.COLUMN_TOKEN + " TEXT,"
//                    + DataContract.Preference.COLUMN_SETTING + " TEXT,"
//                    + DataContract.Preference.COLUMN_POSITION_CATEGORY + " INTEGER "
//                    + ")";
//            statement.execute(sql);
//        } catch (SQLException e) {
//        }
//    }



//    private void createTableTransaction(Statement statement) {
//        try {
//            String sql = "CREATE TABLE IF NOT EXISTS " + DataContract.Transaction.TABLE_NAME
//                    + " (" + DataContract.Transaction.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
//                    + DataContract.Transaction.COLUMN_ID_SERVER + " INTEGER,"
//                    + DataContract.Transaction.COLUMN_AMAZON_ORDER_ID + " VARCHAR,"
//                    + DataContract.Transaction.COLUMN_AMAZON_ORDER_ITEM_ID + " VARCHAR,"
//                    + DataContract.Transaction.COLUMN_ASIN + " VARCHAR,"
//                    + DataContract.Transaction.COLUMN_SKU + " VARCHAR,"
//                    + DataContract.Transaction.COLUMN_PRODUCT_NAME + " VARCHAR,"
//                    + DataContract.Transaction.COLUMN_ORDER_STATUS + " VARCHAR,"
//                    + DataContract.Transaction.COLUMN_PRICE_AMAZON + " INTEGER,"
//                    + DataContract.Transaction.COLUMN_SHIPPING_PRICE + " INTEGER,"
//                    + DataContract.Transaction.COLUMN_QUANTITY_ORDER + " INTEGER,"
//                    + DataContract.Transaction.COLUMN_BUYER_NAME + " VARCHAR,"
//                    + DataContract.Transaction.COLUMN_SHIPPING_ADDRESS + " VARCHAR,"
//                    + DataContract.Transaction.COLUMN_CONDITION_NOTE + " TEXT,"
//                    + DataContract.Transaction.COLUMN_CURRENT_PRICE_YAHOO + " INTEGER,"
//                    + DataContract.Transaction.COLUMN_PURCHASE_PRICE + " INTEGER,"
//                    + DataContract.Transaction.COLUMN_BUY_NOW_PRICE + " INTEGER,"
//                    + DataContract.Transaction.COLUMN_AUCTION_PROFIT + " INTEGER,"
//                    + DataContract.Transaction.COLUMN_PROFIT + " INTEGER,"
//                    + DataContract.Transaction.COLUMN_BUY_NOW_PROFIT + " INTEGER,"
//                    + DataContract.Transaction.COLUMN_TIME_EXPIRED + " INTEGER,"
//                    + DataContract.Transaction.COLUMN_STATUS + " INTEGER,"
//                    + DataContract.Transaction.COLUMN_SELLER_AUCTION + " VARCHAR,"
//                    + DataContract.Transaction.COLUMN_IMAGE_PRODUCT + " TEXT,"
//                    + DataContract.Transaction.COLUMN_PURCHASE_DATE_AMAZON + " INTEGER,"
//                    + DataContract.Transaction.COLUMN_DETAIL_YAHOO_PRODUCT + " TEXT"
//                    + ")";
//            statement.execute(sql);
//        } catch (SQLException e) {
//            print("SQLException createTableTransaction " + e.toString());
//        }
//    }


//    public void insertPreference(Preference preference, boolean isLogin) {
//        int check = isLogin ? 1 : 0;
//        try {
//            Statement statement = connection.createStatement();
//            String sql = "INSERT OR IGNORE INTO " + DataContract.Preference.TABLE_NAME
//                    + " ("
//                    + DataContract.Preference.COLUMN_USER_ID + ","
//                    + DataContract.Preference.COLUMN_IS_LOGIN + ","
//                    + DataContract.Preference.COLUMN_HOST + ","
//                    + DataContract.Preference.COLUMN_SETTING
//                    + ")"
//                    + " VALUES"
//                    + " ("
//                    + preference.getUserId() + ","
//                    + check + ","
//                    + "'" + preference.getHost() + "',"
//                    + "'" + preference.getSetting() + "'"
//                    + ")";
//            statement.execute(sql);
//        } catch (SQLException e) {
//            System.out.printf("SQLException " + e.toString());
//        }
//    }

//    public synchronized void updatePreference(String accessToken) {
//        try {
//            Statement statement = connection.createStatement();
//            String sql = "UPDATE " + DataContract.Preference.TABLE_NAME
//                    + " SET "
//                    + DataContract.Preference.COLUMN_TOKEN + "=" + "'" + accessToken + "'";
//            statement.execute(sql);
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void updatePreference(Preference preference) {
//        try {
//            Statement statement = connection.createStatement();
//            String sql = "UPDATE " + DataContract.Preference.TABLE_NAME
//                    + " SET "
//                    + DataContract.Preference.COLUMN_SETTING + "=" + "'" + preference.getSetting() + "',"
//                    + DataContract.Preference.COLUMN_POSITION_CATEGORY + "=" + preference.getPositionCategory();
//            statement.execute(sql);
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
//
//
//    public boolean isLogin() {
//        try {
//            Statement statement = connection.createStatement();
//            String sql = "SELECT * FROM " + DataContract.Preference.TABLE_NAME;
//            ResultSet resultSet = statement.executeQuery(sql);
//            if (resultSet != null && !resultSet.isClosed()) {
//                return resultSet.getInt(DataContract.Preference.COLUMN_IS_LOGIN) == 1;
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return false;
//    }
//
//    public synchronized Preference getPreference() {
//        try {
//            Statement statement = connection.createStatement();
//            String sql = "SELECT * FROM " + DataContract.Preference.TABLE_NAME;
//            ResultSet resultSet = statement.executeQuery(sql);
//            if (resultSet != null && !resultSet.isClosed()) {
//                Preference preference = new Preference();
//                preference.setUserId(resultSet.getInt(DataContract.Preference.COLUMN_USER_ID));
//                preference.setHost(resultSet.getString(DataContract.Preference.COLUMN_HOST));
//                preference.setAccessToken(resultSet.getString(DataContract.Preference.COLUMN_TOKEN));
//                preference.setSetting(resultSet.getString(DataContract.Preference.COLUMN_SETTING));
//                preference.setPositionCategory(resultSet.getInt(DataContract.Preference.COLUMN_POSITION_CATEGORY));
//                return preference;
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }



    public void closeConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
        }
    }

    public void clearDatabase() {
        try {
            Statement statement = connection.createStatement();
            statement.execute("DELETE FROM " + DataContract.Preference.TABLE_NAME);
        } catch (SQLException e) {
        }
    }
}
