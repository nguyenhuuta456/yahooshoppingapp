package app;

import app.database.DatabaseHelper;
import app.entity.Preference;
import app.stage.LoginAppStage;
import app.stage.MainStage;
import app.task.uploadtask.BackgroundServices;
import app.utils.Constants;
import app.utils.Logger;
import app.utils.Utils;
import app.utils.WebDriverUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.org.apache.xerces.internal.impl.io.UTF8Reader;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriverService;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class App extends Application {
    private static final String TAG = "App";
    public static App instance;
    private Gson gson;
    private Stage stageApp;
    private static final DatabaseHelper DATABASE;
    private Preference preference;
    private List<IListenerStopApp> stopAppList;

    public static boolean debug = false;
    public static String url = "";

    static {
        DATABASE = DatabaseHelper.getInstance();
        String userDir = System.getProperty("user.dir");
        setupWebDriver(userDir);
        setupLog4J(userDir);
        Logger.setup();
    }

    private static void setupLog4J(String userDir) {
        File logConfig = new File(userDir + "/" + Constants.LOG4J_CONFIG_FILE_NAME);
        System.setProperty("log4j.configurationFile", logConfig.getPath());
    }

    private static void setupWebDriver(String userDir) {
        if (SystemUtils.IS_OS_WINDOWS) {
            System.setProperty(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY, userDir + "/webdriver/chromedriver.exe");
        } else if (SystemUtils.IS_OS_MAC) {
            File webDriver = new File(userDir, "/webdriver/chromedriver");
            Utils.grantFilePermission(webDriver);
            System.setProperty(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY, userDir + "/webdriver/chromedriver");
        }
    }

    @Override
    public void start(Stage primaryStage) {
        gson = new Gson();
        DATABASE.onUpgrade();
        stageApp = primaryStage;
        instance = this;
        preference = getPreference();
        stopAppList = new ArrayList<>();
        if (preference == null) {
            new LoginAppStage(primaryStage);
        } else {
            new MainStage(primaryStage);
        }
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        Logger.d(TAG, "#stop App");
        notifyStopApp();
        System.exit(0);
        Platform.exit();
    }

    public static void main(String[] args) {
        if (args != null && args.length > 0) {
            debug = true;
            url = args[0];
        }
        launch(args);
    }

    public Stage getStageApp() {
        return stageApp;
    }

    public static App getInstance() {
        return instance;
    }

    public Preference getPreference() {
        if (preference == null) {
            BufferedReader bw;
            try {
                bw = new BufferedReader(new UTF8Reader(new FileInputStream(DatabaseHelper.DIRECTORY + "/" + DatabaseHelper.PREFERENCE)));
                String jsonPreference = bw.readLine();
                if (jsonPreference != null && !jsonPreference.isEmpty()) {
                    preference = gson.fromJson(jsonPreference, Preference.class);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return preference;
    }

    public void setPreference(Preference preference) {
        this.preference = preference;
    }

    public void savePreference(Preference preference) {
        String json = preference == null ? Constants.EMPTY : gson.toJson(preference);
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(DatabaseHelper.DIRECTORY + "/" + DatabaseHelper.PREFERENCE, false), StandardCharsets.UTF_8))) {
            bw.write(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Set<Cookie> getCookieFromFile() {
        Set<Cookie> cookies = new HashSet<>();
        BufferedReader bw;
        try {
            bw = new BufferedReader(new UTF8Reader(new FileInputStream(DatabaseHelper.DIRECTORY + "/" + DatabaseHelper.COOKIE)));
            String cookie = bw.readLine();
            if (cookie != null && !cookie.isEmpty()) {
                cookies = gson.fromJson(cookie, new TypeToken<Set<Cookie>>() {
                }.getType());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cookies;
    }


    public void saveCookieToFile(Set<Cookie> cookies) {
        String json = cookies == null ? Constants.EMPTY : gson.toJson(cookies);
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(DatabaseHelper.DIRECTORY + "/" + DatabaseHelper.COOKIE, false), StandardCharsets.UTF_8))) {
            bw.write(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void registerListenerStopApp(IListenerStopApp stopApp) {
        if (!stopAppList.contains(stopApp)) {
            stopAppList.add(stopApp);
        }
    }

    public void notifyStopApp() {
        for (IListenerStopApp stopApp : stopAppList) {
            stopApp.onStopApp();
        }
    }

    public interface IListenerStopApp {
        void onStopApp();
    }
}
