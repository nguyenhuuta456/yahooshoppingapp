(function() {
      
    var isIE;

    /* -------------------------------
    *  Utility methods
    * ------------------------------- */
    
    function bind(method, obj) {

        var args = Array.prototype.splice.call(arguments,[2]);
        return function(){
            return method.apply(obj, args);
        };
    }
    
    function getXMLReqObj() {

        var xmlReqObj = null;
        if (window.ActiveXObject) {

            // Code for IE
            xmlReqObj = new ActiveXObject("Microsoft.XMLHTTP");
            isIE = true;

        } else {

            // Mozilla based
            xmlReqObj = new XMLHttpRequest();
            isIE = false;
        }

        return xmlReqObj;
    }

    function getXmlDomObj(sourceXml) {

        var xmlDoc;

        if (window.ActiveXObject) {

        // Code for IE
            if (typeof(xmlDoc) == 'undefined') {

                xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
                xmlDoc.async="false";
                isIE=true;
            }

            xmlDoc.loadXML(sourceXml);

        } else {

            // Mozilla based
            var parser=new DOMParser();
            xmlDoc=parser.parseFromString(sourceXml,"text/xml");
            isIE=false;
        }

        return xmlDoc;
    }

    function getValueByTagName(name, parent) {
        
        var par = parent || document,
            tag = par.getElementsByTagName(name);
            
        if (tag && tag.length) {
            return tag[0].firstChild.nodeValue;
        }
        
        return '';
    }
    
    /**
    * @class Captcha
    */
    function Captcha(obj) {
        this.prefix = obj.prefix;
        this.id = obj.id;
        this.ajaxUrl = obj.proxyAddr;
        this.isShuffling = null;
        this.captchaView = null;
    }
    
    Captcha.prototype.makeRequest = function(url, callback, scope) {

        var xmlReqObj = getXMLReqObj();  

        xmlReqObj.open("GET",url,true);
        xmlReqObj.setRequestHeader('Content-Type', 'text/xml');
        xmlReqObj.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT" );

        xmlReqObj.onreadystatechange= function() { 

            if (xmlReqObj.readyState==4) { 

                callback.success.call(scope,xmlReqObj);
            } 
        };

        xmlReqObj.send(null);        
    };
    
    Captcha.prototype.addCaptchaEvents = function() {
        
        var buttonsObj = document.getElementById(this.prefix + "Content");

        if (!!buttonsObj) {
            var buttons = buttonsObj.getElementsByTagName("span"),
                length = buttons.length;

            while(length--){
                if (buttons[length].className === "captchaButTry"){
                    buttons[length].onclick = bind( this.onShuffleCaptcha,this);
                }
            }

            // 2011/02/21 add  ( document.getElementById( this.prefix + "Header" ) != null ) check 
            if( document.getElementById( this.prefix + "Header" ) != null ){
              document.getElementById( this.prefix + "Header" ).getElementsByTagName("a")[0].onclick = this.Help;
            }
        }
    };
    
    Captcha.prototype.detachCaptchaEvents = function() {
    
        var buttonsObj = document.getElementById(this.prefix + "Content");

        if (!!buttonsObj) {

            var buttons = buttonsObj.getElementsByTagName("test"),
                length = buttons.length;

            while(length--){
                if (buttons[length].className === "captchaButTry"){
                    buttons[length].onclick = null;
                }
            }
    
            // 2011/02/21 add  ( document.getElementById( this.prefix + "Header" ) != null ) check 
            if( document.getElementById( this.prefix + "Header" ) != null ){
              document.getElementById( this.prefix + "Header" ).getElementsByTagName("a")[0].onclick = this.Help;
            }
        }    
        
    };
    
    Captcha.prototype.Help = function(e) {
        
        e = e || event;
        var targ = e.target || e.srcElement;

        //window.open(targ.href, "help", "width=500,height=585,scrollbars=yes,dependent=yes");
        window.open("http://captcha.yahoo.co.jp/help/captcha_help.php", "help", "width=750,scrollbars=yes,dependent=yes");
        
        if (e.preventDefault) {
            e.preventDefault();
        } else {
            e.returnValue = false;
        }
        
        return false;
    };
    
    Captcha.prototype.loadShuffleResult = function(resp) {

        var statusNode = new Array();  //IE bugfix Y!JP
        var newXml = resp.responseText,
            newXmlDoc = getXmlDomObj(newXml),
            statusNode = newXmlDoc.getElementsByTagName("Code"),
            statusResp,
            turnkeyResp,
            exemptNode,
            turnKeyNode;

        if (!newXmlDoc || !statusNode) {
            statusResp = "0";
        } else {
            statusResp = isIE ? statusNode[0].text : statusNode[0].textContent;
        }

        if (statusResp !== "0") {

            // Note: not clear how we want to deal with error in shuffle,
            //       most likely case is "exempt" on lazy load
            //       for now, just return the error msg, which
            //       is non-sensical as this function is called
            //       asynchronously (ajax wise)

            exemptNode = newXmlDoc.getElementsByTagName("Msg");
            turnkeyResp = isIE ? exemptNode[0].text : exemptNode[0].textContent;

        } else {

            turnkeyNode = newXmlDoc.getElementsByTagName("Turnkey");

            if (turnkeyNode && turnkeyNode.length) {

                //turnkey mode
                turnkeyResp = isIE ? turnkeyNode[0].text : turnkeyNode[0].textContent;

                turnkeyResp = turnkeyResp.replace('<div id="'+this.prefix+'">','');
                turnkeyResp = turnkeyResp.replace('</div> <!--'+this.prefix+'-->','');

                document.getElementById(this.prefix).innerHTML = turnkeyResp;

            } else {

                //data mode
                var dataNode = newXmlDoc.getElementsByTagName('Data')[0],
                    captchaTypesNode = dataNode.getElementsByTagName('CaptchaTypes'),
                    captchaType  = dataNode.getElementsByTagName('Type'), // for new multibyte captcha xml format
                    captchaTypes = (captchaTypesNode && captchaTypesNode.length) ? captchaTypesNode[0].firstChild.nodeValue.split(',') : [];
                // for new multibyte captcha xml format
                if( !captchaTypes.length && captchaType.length){
                    captchaTypes[0] = captchaType[0].firstChild.nodeValue;
                }

                for (var i=0,j=captchaTypes.length; i<j; i++) {
                    var typeId = captchaTypes[i],
                        captchaNode = dataNode.getElementsByTagName(typeId)[0],
                        testId = getValueByTagName('TestId', captchaNode),
                        idNode = document.getElementById(this.prefix + typeId + "Id");

                    if (!!idNode) {
                       idNode.value = testId;
                    }

                    // Additional coding for specific captcha types
                    var imgNode = captchaNode.getElementsByTagName("Img")[0],
                        imgSrcNode = document.getElementById(this.prefix + typeId + "Img");
                    if (!!imgSrcNode) {
                        imgSrcNode.src = getValueByTagName('Url', imgNode);
                    }
                }

                // Load in other hidden Data fields

                var hiddenData = dataNode.getElementsByTagName('HiddenData')[0],
                    hiddenDataFields = (hiddenData && hiddenData.length) ? hiddenData.getElementsByTagName('DataField') : [];
                                    
                for (i=0,j=hiddenDataFields.length; i<j; i++) {

                    var hiddenDataFieldName = getValueByTagName('Name', hiddenDataFields[i]),
                        hiddenDataFieldValue = getValueByTagName('Value', hiddenDataFields[i]);
                    
                    document.getElementById(this.prefix + hiddenDataFieldName).value =  hiddenDataFieldValue;
                }
            }

            // 2011/02/21 add  (YAHOO.CAPTCHA[this.id] != null) check
            // Embed the audio object and autoPlay when ready
            if ( (YAHOO.CAPTCHA[this.id] != null) && typeof(YAHOO.CAPTCHA[this.id].embedAudio) !== 'undefined') {

                if (typeof(this.captchaView) === 'undefined') {
                    this.captchaView = '';
                } 

                YAHOO.CAPTCHA[this.id].embedAudio({
                    url:'',
                    force: this.captchaView === 'audio'
                });
            }

        }

        var inputFld = document.getElementById(this.prefix + "Answer");
        inputFld.focus();

        this.isShuffling = false;
        this.addCaptchaEvents();
        YAHOO.CAPTCHA.broadcast("refresh",this.id);
        
        return statusResp;
    };
    
    Captcha.prototype.onShuffleCaptcha = function(contentMode) {

        var ajaxParams, xmlReqObj;

        //renewShuffleCta
        var Dom = YAHOO.util.Dom;
        var sectry = Dom.get("sectry");
        var sec_num = sectry.value;
        var shuffleCta = document.getElementById("captchaShuffleLink");
        if(sec_num < 10 && shuffleCta.className == "arcshd") {
            sec_num++;
            sectry.value=sec_num;
            if(sec_num >= 10){
                shuffleCta.src = "https://s.yimg.jp/images/reg/ast/secwordBtn-disable.gif";
                shuffleCta.className = "arcsdf";
            }
        } else {
            shuffleCta.src = "https://s.yimg.jp/images/reg/ast/secwordBtn-disable.gif";
            shuffleCta.className = "arcsdf";
            this.detachCaptchaEvents();
            return false; 
        }

        if (typeof(contentMode) === "undefined") contentMode = "data";

        if (typeof(this.isShuffling) !== 'undefined' && this.isShuffling) {
            return; // Hack to avoid double-load in FF
        } 
        this.isShuffling = true; // Set to indicate to Audio Captcha to play on load

        ajaxParams = "captchaCdata=" + document.getElementById(this.prefix+"Cdata").value;
        ajaxParams += "&lang=" + document.getElementById("lang").value;
        ajaxParams += "&content_mode="+contentMode;
        ajaxParams += "&action=shuffle";

        if (document.getElementById(this.prefix+"View") != null ) {
        try {
            this.captchaView = document.getElementById(this.prefix+"View").value;
            ajaxParams += "&initial_view="+this.captchaView;
        } catch (e) {};
        }


        this.makeRequest(this.ajaxUrl+"?"+ajaxParams, {
            success:this.loadShuffleResult
        }, this);
    };
    
    //make sure YAHOO and Captcha are defined
    if (typeof(YAHOO) === "undefined") {YAHOO = {};} 
    if (typeof(YAHOO.CAPTCHA) === "undefined") {YAHOO.CAPTCHA = {};}
    if (typeof(YAHOO.CAPTCHA.items) === "undefined") {YAHOO.CAPTCHA.items = [];}
    if (typeof(YAHOO.CAPTCHA.events) === "undefined") {YAHOO.CAPTCHA.events = {};}
    
    YAHOO.CAPTCHA.broadcast = function(eventName, constraint) {

        if (typeof(YAHOO.CAPTCHA.events[eventName]) !== "undefined") {
            
            var subs = YAHOO.CAPTCHA.events[eventName].subscribers,
                index,
                len = subs.length,
                check = constraint || "";
            
            for (index = 0; index < len; index++) {
                if (subs[index].constraint === check) {
                    subs[index].callback();
                }
            }
        }
    };
    
    YAHOO.CAPTCHA.subscribe = function(eventName, func, constraint) {

        if (typeof(YAHOO.CAPTCHA.events[eventName]) === "undefined") { 
            YAHOO.CAPTCHA.events[eventName] = {
                subscribers:[]
            };
        }
        YAHOO.CAPTCHA.events[eventName].subscribers.push({
            "constraint":constraint || "",
            "callback":func
        });
    };
    
    YAHOO.CAPTCHA.destruct = function(id) {
        
        var cid = id || "V5",
            items = YAHOO.CAPTCHA.items,
            length = items.length;
        
        YAHOO.CAPTCHA.broadcast("destruct", cid);
        
        while(length--) {
            if (items[length].id === cid) {
                YAHOO.CAPTCHA.items[length] = null;
                YAHOO.CAPTCHA.items.splice(length, 1);
                break;
            }
        }
        
    };
    
    YAHOO.CAPTCHA.addCaptcha = function(params) {
        var id = params.id || "V5",
            len = YAHOO.CAPTCHA.items.length;
            lazyload = (typeof(params.lazyload) !== 'undefined') ? params.lazyload : false;

        YAHOO.CAPTCHA.items[len] = new Captcha({
            "id": id,
            "prefix": params.prefix || "captcha"+id,
            "proxyAddr": params.proxyAddr
        });
        
        YAHOO.CAPTCHA.items[len].addCaptchaEvents();
        YAHOO.CAPTCHA.subscribe("toggle", bind(YAHOO.CAPTCHA.items[len].addCaptchaEvents, YAHOO.CAPTCHA.items[len]), id);
        YAHOO.CAPTCHA.subscribe("destruct", bind(YAHOO.CAPTCHA.items[len].detachCaptchaEvents, YAHOO.CAPTCHA.items[len]), id);
        
        if (lazyload) YAHOO.CAPTCHA.items[len].onShuffleCaptcha("turnkey");

        return len;        
    };

    YAHOO.CAPTCHA.addShuffleButton = function() {
        var Dom = YAHOO.util.Dom;
        var Event = YAHOO.util.Event;
        var Conn = YAHOO.util.Connect;
        var sectry = Dom.get("sectry");
        var container = Dom.get("captchaShuffler");

        var shuffleCta = document.createElement("img");
        var secnum = sectry.value;
        if(secnum == ""){
            secnum = 0;
            sectry.value = secnum;
        }
        //renew chuffleCta 
        if(container !== null){
            shuffleCta.id = "captchaShuffleLink";
            if(secnum < 10){
                shuffleCta.src = "https://s.yimg.jp/images/reg/ast/secwordBtn-actv.gif";
                shuffleCta.className = "arcshd";
            }else{
                shuffleCta.src = "https://s.yimg.jp/images/reg/ast/secwordBtn-disable.gif";
                shuffleCta.className = "arcsdf";
            }
            shuffleCta.alt = "別の画像を表示する";
            container.appendChild(shuffleCta);
        }
    };  
    
})();


