// reg Event Listener
function addListener(oElm, sType, func, cap) {
	if (!oElm) { return false; }
	if (oElm.addEventListener) { // W3 browser
		oElm.addEventListener(sType, func, cap);
} else if (oElm.attachEvent) { // IE
		oElm.attachEvent("on"+sType, func);
} else {
		return false;
}
	return false;
}

// change color on focus
function focusForm() {
	if (document.getElementById('errTitle')) return false;
  if (document.getElementsByTagName) {
    var input = document.getElementsByTagName('input');
    for (var i=0; i < input.length; i++) {
      if (input[i].getAttribute('type') == 'text' || input[i].getAttribute('type') == 'password') {
        input[i].onfocus = function() {
          this.style.backgroundColor = '#FEFCD8';
          return false;
        };
				 input[i].onblur = function() {
          this.style.backgroundColor = '#ffffff';
          return false;
        };
      }
    }
  }
}
addListener(window, 'load', focusForm, false);
