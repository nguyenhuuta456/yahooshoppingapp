package app.stage;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class LoginAppStage extends Stage {
    public LoginAppStage(Stage primaryStage) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/layout/layout_login_app.fxml"));
            primaryStage.setTitle("Login");
            primaryStage.setScene(new Scene(root, 600, 400));
            primaryStage.setWidth(600);
            primaryStage.setHeight(400);
            primaryStage.setMinWidth(600);
            primaryStage.setMinHeight(400);
            primaryStage.centerOnScreen();
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
