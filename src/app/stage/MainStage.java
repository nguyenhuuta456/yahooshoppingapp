package app.stage;

import javafx.animation.FadeTransition;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;

public class MainStage extends Stage {
    public MainStage(Stage stage) {
        try {
//            stage.hide();
            Parent root = FXMLLoader.load(getClass().getResource("/layout/layout_main.fxml"));
            stage.setTitle("Yahoo");
            FadeTransition ft = new FadeTransition(Duration.millis(500), root);
            ft.setFromValue(0.0);
            ft.setToValue(1.0);
            ft.play();
            stage.setScene(new Scene(root));
            stage.setMinWidth(1080);//1080
            stage.setMinHeight(720);//720
            stage.setWidth(1080);
            stage.setHeight(720);
            stage.setResizable(true);
            stage.centerOnScreen();
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
