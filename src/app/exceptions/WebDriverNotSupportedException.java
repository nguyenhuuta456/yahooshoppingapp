package app.exceptions;

/**
 * Created by HieuPT on 4/16/2018.
 */
public class WebDriverNotSupportedException extends Exception {

    public WebDriverNotSupportedException(String message) {
        super(message);
    }
}
