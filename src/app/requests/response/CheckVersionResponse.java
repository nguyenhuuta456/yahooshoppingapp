package app.requests.response;

import app.requests.BaseResponse;
import com.google.gson.annotations.SerializedName;

public class CheckVersionResponse extends BaseResponse<CheckVersionResponse.Data> {
    public class Data {
        @SerializedName("id")
        private int id;

        @SerializedName("version_code")
        private String versionCode;

        @SerializedName("link_download")
        private String linkDownLoad;

        public int getId() {
            return id;
        }

        public String getVersionCode() {
            return versionCode;
        }

        public String getLinkDownLoad() {
            return linkDownLoad;
        }
    }
}
