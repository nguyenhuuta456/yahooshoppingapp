package app.requests.response;

import app.requests.BaseResponse;
import com.google.gson.annotations.SerializedName;

public class LoginResponse extends BaseResponse<LoginResponse.LoginData> {
    public class LoginData {

        @SerializedName("id")
        private String accountId;

        @SerializedName("yahoo_auction_username")
        private String accountYahoo;

        @SerializedName("password")
        private String password;

        public String getAccountId() {
            return accountId;
        }

        public String getAccountYahoo() {
            return accountYahoo;
        }

        public String getPassword() {
            return password;
        }
    }
}
