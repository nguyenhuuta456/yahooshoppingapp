package app.requests;

import app.App;
import app.entity.Preference;
import app.requests.params.*;
import app.requests.response.CheckVersionResponse;
import app.requests.response.ListAsinResponse;
import app.requests.response.LoginResponse;
import app.requests.response.ProductResponse;
import app.task.IBaseTaskCallback;
import app.utils.Constants;
import retrofit2.Call;

import java.util.List;

public class YahooShoppingModel extends BaseModel {

    private String host;
    private String accountId;

    public YahooShoppingModel() {
        Preference preference = App.getInstance().getPreference();
        if (preference != null) {
            host = preference.getHost();
            accountId = preference.getAccountId();
        }
    }

    public void loginApp(String host, String userName, String password, IBaseCallback<LoginResponse> callback) {
        Call<LoginResponse> call = INSTANCE_COMMON(host).login(new LoginParams(userName, password));
        Request().executeRequest(call, callback);
    }

    public void checkVersionApp(IBaseCallback<CheckVersionResponse> callback) {
        String host = "http://133.130.73.227:6789/";
        Call<CheckVersionResponse> call = INSTANCE_COMMON(host).checkVersion(new CheckVersionParams(Constants.APP_VERSION_CODE));
        Request().executeRequest(call, callback);
    }


    public void updateAccount(String host, UpdateAccountParams params, IBaseCallback<BaseResponse> callback) {
        Call<BaseResponse> call = INSTANCE_COMMON(host).updateAccount(params);
        Request().executeRequest(call, callback);
    }

    public void getListProductUpload(String host, String accountId, IBaseCallback<ProductResponse> callback) {
        Call<ProductResponse> call = INSTANCE_COMMON(host).getListProductUpload(accountId);
        Request().executeRequest(call, callback);
    }

    public void updateProduct(String host, UpdateProductParams params, IBaseCallback<BaseResponse> callback) {
        Call<BaseResponse> call = INSTANCE_COMMON(host).updateProduct(params);
        Request().executeRequest(call, callback);
    }

    public void handleErrorProduct(String host, HandlerErrorProductParams params, IBaseCallback<BaseResponse> callback) {
        Call<BaseResponse> call = INSTANCE_COMMON(host).handleErrorProduct(params);
        Request().executeRequest(call, callback);
    }

    public void getAsinsByItemCode(GetAsinParams params, IBaseCallback<ListAsinResponse> callback) {
        Call<ListAsinResponse> call = INSTANCE_COMMON(host).getListAsin(params);
        Request().executeRequest(call, callback);
    }

    public void deleteProduct(List<String> listItemCode, IBaseCallback<BaseResponse> callback) {
        Call<BaseResponse> call = INSTANCE_COMMON(host).deleteProduct(new DeleteParams(accountId, listItemCode));
        Request().executeRequest(call, callback);
    }

    public void saveCookie(String json, IBaseCallback<BaseResponse> callback) {
        Call<BaseResponse> call = INSTANCE_COMMON(host).saveCookie(new CookiesParams(json, accountId));
        Request().executeRequest(call, callback);
    }
}
