package app.requests;

import app.utils.Utils;
import javafx.application.Platform;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class RequestQueue {

    public static final String BASE_URL_LOGIN = "http://133.130.73.227:6789/";

    private static final String BASE_URL_YAHOO = "https://auctions.yahooapis.jp/";
    public static final String BASE_URL_SECURE_YAHOO = "https://secure.auctions.yahooapis.jp/";
    private static final String BASE_URL_MAP_YAHOO = "https://map.yahooapis.jp/";


    private static String baseUrl = "";

    private static Retrofit retrofitCommon = null;


    private static OkHttpClient.Builder httpClient;


    private static Retrofit getClient(String baseUrl) {
        if (!RequestQueue.baseUrl.equals(baseUrl)) {
            RequestQueue.baseUrl = baseUrl;
            changeApiBaseUrl(baseUrl);
        }
        if (retrofitCommon == null) {
            initOkHttp();
            retrofitCommon = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofitCommon;
    }

    static void changeApiBaseUrl(String newApiBaseUrl) {
        initOkHttp();
        baseUrl = newApiBaseUrl;
        retrofitCommon = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }




    private synchronized static void initOkHttp() {
        if (httpClient == null) {
            httpClient = new OkHttpClient.Builder()
                    .readTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(60, TimeUnit.SECONDS);
            httpClient.addInterceptor(chain -> {
                Request request = chain.request().newBuilder().addHeader("Accept", "application/json").build();
                return chain.proceed(request);
            });
        }
    }

    public static IYahooServices INSTANCE_COMMON(String baseUrl) {
        return getClient(baseUrl).create(IYahooServices.class);
    }


    public <T extends BaseResponse> void executeRequest(Call<T> call, IBaseCallback<T> callback) {
        call.enqueue(new Callback<T>() {

            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                Platform.runLater(() -> {
                    T body = response.body();
                    if (body != null) {
                        if (body.isSuccess()) {
                            callback.onResponse(body);
                        } else {
                            String message = body.getMessage();
                            callback.onError(message);
                        }
                    } else {
                        callback.onError(Utils.getStringError(response.errorBody()));
                    }
                });
            }

            @Override
            public void onFailure(Call<T> call, Throwable throwable) {
                Platform.runLater(() -> callback.onError(throwable.getMessage()));
            }
        });
    }

    public <T> void executeRequests(Call<T> call, IBaseCallback<T> callback) {
        call.enqueue(new Callback<T>() {

            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                Platform.runLater(() -> {
                    T body = response.body();
                    if (body != null) {
                        callback.onResponse(body);
                    } else {
                        callback.onError(Utils.getStringError(response.errorBody()));
                    }

                });
            }

            @Override
            public void onFailure(Call<T> call, Throwable throwable) {
                Platform.runLater(() -> callback.onError(throwable.getMessage()));
            }
        });
    }

    public <T> boolean executeASyncRequest(Call<T> call, IBaseCallback<T> callback) {
        try {
            Response<T> response = call.execute();
            String errorMesg = "Unknow";
            if (response.isSuccessful()) {
                T t = response.body();
                if (t == null) {
                    if (callback != null)
                        callback.onError("Response null");
                    return false;
                }
                if (callback != null)
                    callback.onResponse(t);
                return true;
            } else {
                ResponseBody errorBody = response.errorBody();
                if (errorBody != null) {
                    errorMesg = errorBody.string();
                }
            }
            if (callback != null)
                callback.onError(errorMesg);
            return false;
        } catch (IOException e) {
            callback.onError("Unknow");
            return false;
        }
    }
}
