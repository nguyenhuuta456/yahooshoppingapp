package app.requests;

public abstract class BaseModel {

    private RequestQueue requestQueue;

    public IYahooServices INSTANCE_COMMON(String baseUrl) {
        return RequestQueue.INSTANCE_COMMON(baseUrl);
    }

    public RequestQueue Request() {
        if (requestQueue == null) {
            requestQueue = new RequestQueue();
        }
        return requestQueue;
    }


}
