package app.requests.params;

import com.google.gson.annotations.SerializedName;

public class CheckVersionParams {
    @SerializedName("version_code")
    private String version_code;

    @SerializedName("type")
    private int type;

    public CheckVersionParams(String version_code) {
        this.version_code = version_code;
        this.type = 2;
    }
}
