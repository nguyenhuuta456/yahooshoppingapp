package app.requests.params;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DeleteParams extends AccountParams {

    public DeleteParams(String accountId, List<String> items) {
        super(accountId);
        this.listItemCode = items;
    }

    @SerializedName("data")
    private List<String> listItemCode;
}
