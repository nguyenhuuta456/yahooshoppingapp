package app.requests.params;

import app.entity.ProductUpload;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class HandlerErrorProductParams {
    public static final int CATEGORY_ERROR_CODE = 1;

    public HandlerErrorProductParams(String accountId, ProductUpload productUpload, int code) {
        this.accountId = accountId;
        listId = new ArrayList<>();
        listId.add(productUpload.getId());
        this.code = code;
    }

    @SerializedName("account_id")
    private String accountId;
    @SerializedName("list_id_product_error")
    private List<String> listId;
    @SerializedName("error_code")
    private int code;

}
