package app.requests.params;

import app.entity.ProductUpload;
import app.utils.Constants;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class UpdateProductParams {

    public UpdateProductParams(String accountId, ProductUpload productUpload) {
        this.accountId = accountId;
        this.list = new ArrayList<>();
        Data data = new Data(productUpload);
        this.list.add(data);
    }

    @SerializedName("account_id")
    private String accountId;

    @SerializedName("data")
    private List<Data> list;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public List<Data> getList() {
        return list;
    }

    public void setList(List<Data> list) {
        this.list = list;
    }

    public class Data {

        public Data(ProductUpload productUpload) {
            this.id = productUpload.getId();
            this.itemCode = productUpload.getItemCode();
            this.price = productUpload.getPrice();
            this.quantity = productUpload.getQuantity();
            this.publishStatus = productUpload.getPublishStatus();
            if (publishStatus == ProductUpload.UPLOAD_FAILURE) {
                this.errorMessage = productUpload.getMessageFailure();
            } else {
                this.errorMessage = Constants.EMPTY;
            }
        }

        @SerializedName("id")
        private String id;
        @SerializedName("item_code")
        private String itemCode;
        @SerializedName("price")
        private int price;
        @SerializedName("quantity")
        private int quantity;
        @SerializedName("publish_status")
        private int publishStatus;
        @SerializedName("publish_error_message")
        private String errorMessage;
    }
}
