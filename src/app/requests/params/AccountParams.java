package app.requests.params;

import com.google.gson.annotations.SerializedName;

public class AccountParams {

    public AccountParams(String accountId) {
        this.accountId = accountId;
    }

    @SerializedName("account_id")
    private String accountId;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
}
