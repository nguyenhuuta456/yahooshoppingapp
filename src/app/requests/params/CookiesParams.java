package app.requests.params;

import com.google.gson.annotations.SerializedName;

public class CookiesParams extends AccountParams {
    public CookiesParams(String cookies, String accountId) {
        super(accountId);
        this.cookies = cookies;
    }

    @SerializedName("cookie")
    private String cookies;

}
