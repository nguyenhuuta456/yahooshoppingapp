package app.requests.params;

import com.google.gson.annotations.SerializedName;

public class UpdateAccountParams {

    public UpdateAccountParams(String accountId, String userName, String password, String shopId) {
        this.accountId = accountId;
        this.data = new Data(userName, password, shopId);
    }

    @SerializedName("account_id")
    private String accountId;
    @SerializedName("data")
    private Data data;


    public String getAccountId() {
        return accountId;
    }

    public Data getData() {
        return data;
    }

    public class Data {
        public Data(String userName, String password, String shopId) {
            this.userName = userName;
            this.password = password;
            this.shopId = shopId;
            String[] split = userName.split("@");
            if (split.length > 0) {
                auctionId = split[0];
            } else {
                auctionId = userName;
            }
        }

        @SerializedName("yahoo_auction_id")
        String auctionId;
        @SerializedName("yahoo_auction_username")
        String userName;
        @SerializedName("password")
        String password;
        @SerializedName("yahoo_shopping_seller_id")
        String shopId;


        public String getUserName() {
            return userName;
        }

        public String getPassword() {
            return password;
        }

        public String getShopId() {
            return shopId;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "auctionId='" + auctionId + '\'' +
                    ", userName='" + userName + '\'' +
                    ", password='" + password + '\'' +
                    ", shopId='" + shopId + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "UpdateAccountParams{" +
                "accountId='" + accountId + '\'' +
                ", data=" + data +
                '}';
    }
}
