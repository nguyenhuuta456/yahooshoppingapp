package app.requests.params;

import com.google.gson.annotations.SerializedName;

public class LoginParams {
    public LoginParams(String account, String password) {
        this.account = account;
        this.password = password;
    }

    @SerializedName("username")
    private String account;

    @SerializedName("password")
    private String password;
}
