package app.requests.params;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetAsinParams extends AccountParams {

    public GetAsinParams(String accountId, List<String> codes) {
        super(accountId);
        this.listItemCode = codes;
    }

    @SerializedName("data")
    private List<String> listItemCode;

    @Override
    public String getAccountId() {
        return super.getAccountId();
    }

    @Override
    public void setAccountId(String accountId) {
        super.setAccountId(accountId);
    }
}
