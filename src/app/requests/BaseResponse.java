package app.requests;

import com.google.gson.annotations.SerializedName;

public class BaseResponse<T> {
    @SerializedName("result")
    private String result;


    @SerializedName("data")
    private T data;

    @SerializedName("message")
    private String message;


    public boolean isSuccess() {
        return result.equalsIgnoreCase("success") || result.equalsIgnoreCase("ok");
    }

    public T getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }
}
