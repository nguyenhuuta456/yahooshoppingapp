package app.requests;

import app.requests.params.*;
import app.requests.response.CheckVersionResponse;
import app.requests.response.ListAsinResponse;
import app.requests.response.LoginResponse;
import app.requests.response.ProductResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

import java.util.List;

public interface IYahooServices {

    @POST("/api/settings/getVersionApp")
    Call<CheckVersionResponse> checkVersion(@Body CheckVersionParams params);

    @POST("/api/users/loginAppYs")
    Call<LoginResponse> login(@Body LoginParams loginRequest);

    @POST("/api/accounts/update_account")
    Call<BaseResponse> updateAccount(@Body UpdateAccountParams params);

    @FormUrlEncoded
    @POST("/api/yahoo-shopping/get-product-list")
    Call<ProductResponse> getListProductUpload(@Field("account_id") String accountId);

    @POST("/api/yahoo-shopping/update-product")
    Call<BaseResponse> updateProduct(@Body UpdateProductParams params);

    @POST("/api/errors/handle-errors")
    Call<BaseResponse> handleErrorProduct(@Body HandlerErrorProductParams params);

    @POST("/api/yahoo-shopping/get-asin-code-by-item-code")
    Call<ListAsinResponse> getListAsin(@Body GetAsinParams params);

    @POST("/api/yahoo-shopping/delete-product-by-item-code")
    Call<BaseResponse> deleteProduct(@Body DeleteParams params);

    @POST("/api/settings/saveCookie")
    Call<BaseResponse> saveCookie(@Body CookiesParams params);


}
