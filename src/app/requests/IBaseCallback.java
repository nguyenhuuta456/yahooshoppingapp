package app.requests;

public interface IBaseCallback<T> {
    void onResponse(T response);

    void onError(String message);
}
