package app;

import app.database.DatabaseHelper;
import app.entity.Preference;
import app.utils.Constants;
import app.utils.Logger;
import app.utils.Utils;
import app.utils.dialog.DialogApp;
import com.google.gson.Gson;
import com.sun.org.apache.xerces.internal.impl.io.UTF8Reader;
import javafx.application.Application;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.image.Image;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import org.apache.xpath.operations.Bool;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static jdk.nashorn.internal.objects.Global.print;

public class AppTest extends Application {
    private String TAG = App.class.getSimpleName();
    ExecutorService executor = Executors.newFixedThreadPool(100);
    int i = 1;

    @Override
    public void start(Stage primaryStage) throws Exception {
        FlowPane root = new FlowPane();

//        String host = "http://133.130:6789/";
//        String host1 = "http://nonstock2.watermeru.com";
//        String[] arr = host1.split("[:\\(//\\)]");
//        String regex = "://(?<addressHost>.*)[:/]*";
//        Pattern pattern = Pattern.compile(regex);
//        Matcher matcher = pattern.matcher(host);
//        while (matcher.find()) {
//            showToast(matcher.group("addressHost"));
//        }
//        showToast("end");
        Preference preference = getPreference();
        List<String> listServer = Constants.Server.getListServer();
        String host = preference.getHost();
        String splitHost = host.split("//")[1];
        String address = splitHost.split("[:/]")[0];
        String findAddress = listServer.parallelStream().filter(server -> {
            String[] splitServer = server.split("_");
            String serverDetail = server;
            if (splitServer.length > 1) {
                serverDetail = splitServer[1];
            }
            return serverDetail.contains(address);
        }).findFirst().orElse("");
        showToast(findAddress);
//        root.setHgap(10);
//        root.setVgap(20);
//        root.setPadding(new Insets(15, 15, 15, 15));
//
//        // Button 1
//        Button button1 = new Button("start");
//        Button button2 = new Button("sleep");
//
//        root.getChildren().addAll(button1, button2);
//        button1.setOnAction(event -> {
//            countDownTimer(60);
//        });
//        button2.setOnAction(event -> {
//            timer.cancel();
//        });
//
//        Scene scene = new Scene(root, 550, 250);
//
//        primaryStage.setTitle("FlowPane Layout Demo");
//        primaryStage.setScene(scene);
//        primaryStage.show();

//
//        String url = "https://creator.shopping.yahoo.co.jp/tvquoajuuk7cqk46n6zlgvkplq/order/ship-send-confirm/";
//        String regex = "OrderID=(?<orderId>.*)";
//        Pattern pattern = Pattern.compile(regex);
//        Matcher matcher = pattern.matcher(url);
//        while (matcher.find()) {
//            System.out.println(matcher.group("orderId"));
//        }


    }

    public Preference getPreference() {
        Preference preference = null;
        Gson gson = new Gson();
        if (preference == null) {
            BufferedReader bw;
            try {
                bw = new BufferedReader(new UTF8Reader(new FileInputStream(DatabaseHelper.DIRECTORY + "/" + DatabaseHelper.PREFERENCE)));
                String jsonPreference = bw.readLine();
                if (jsonPreference != null && !jsonPreference.isEmpty()) {
                    preference = gson.fromJson(jsonPreference, Preference.class);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return preference;
    }

    private int second = 0;

    void showToast(String message) {
        System.out.println(message);
    }


    void print(String message) {
        Logger.d(TAG, message);
    }

    private void countDownTimer(int maxSecond) {
        Logger.d(TAG, "countDownTimer " + maxSecond);
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                second++;
                Logger.d(TAG, String.valueOf(second));
                if (second == maxSecond) {
                    showToast("From countDownTimer");
                    second = 0;
                    timer.cancel();
                } else {
                    showTas("second " + second);
                }
            }
        }, 1000, 1000);
    }

    boolean isStop = false;

    public void timeoutService() throws InterruptedException {
        executor.execute(() -> {
            try {
                int a = 0;
                while (a < 2 && !isStop) {
                    a++;
                    System.out.println("hello ");
                    Thread.sleep(1000);
                }
                executor.shutdown();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        executor.awaitTermination(5, TimeUnit.SECONDS);
        isStop = true;
        System.out.println(">>> Bye bye!");
        Thread.sleep(2000);
        isStop = false;
        timeoutService();
    }

    private Timer timer;

    private void countDown() throws InterruptedException {
        int delay = 1000;
        int period = 1000;
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                second++;
                System.out.println(second);
                if (second == 4) {
                    timer.cancel();
                    second = 0;
                    System.out.println("start");
                }

            }
        }, 1000, 1000);
        Thread.sleep(10000);
        showTas("bye");

    }

    void showTas(String message) {
        System.out.println(message);
    }
}
